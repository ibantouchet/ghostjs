/*jshint strict:false*/
/*global CasperError, console, phantom, require*/

var Ghost = require('casper').Casper,
    system = require('system'),
    env = system.env,
    Casper = require(env["GHOSTJS_PATH"] + "ghost").Casper,
    randomizer = require(env["GHOSTJS_PATH"] + "randomUtils"),
    Mouse = require("mouse"),
    utils = require('utils'),
    x = require('casper').selectXPath,
    fs = require('fs'),
    f  = require('utils').format;




function uniq_arr(a, rgx) {

    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;

    for(var i = 0; i < len; i++) {

        var item = a[i].trim();
        var rawitem = a[i].trim();

        if(rgx!==null && rgx!==undefined && rgx!=="" && (rgx instanceof RegExp || rgx.constructor.name==="RegExp")) {

            rgxres = item.match(rgx)

            if(rgxres !== null)
                item = rgxres[1];
        }

        if(seen[item] !== 1) {
            seen[item] = 1;
            out[j++] = rawitem;
        }
    }
    return out;
}

Casper.prototype._jsonres = {};

Casper.prototype.parsePageData = function(pagename, isfbpage) {

    //isfbpage = (isfbpage===undefined || isfbpage===null || "")? false : isfbpage;
    //isyelppage = /^yelppage-/.test(pagename);

    isfbpage = /www\.facebook\.com/i.test(this.getCurrentPageURLFromEvaluate());
    isyelppage = /yelp\.[a-z]{2,3}(?:\.[a-z]{2,3})?\/biz\//i.test(this.getCurrentPageURLFromEvaluate());

    var xpaths = {
        facebook: "//a[starts-with(@href, 'https://www.facebook.com/')]/@href",
        facebooklink: ["//a[starts-with(@href, 'https://www.facebook.com/')]/@href", {extract: /^(http[^?]+)/}],
        twitter: "//a[starts-with(@href, 'https://twitter.com/')]/@href",
        googlep: "//a[starts-with(@href, 'https://plus.google.com/')]/@href|//a[contains(@href, 'google.com/+')]/@href",
        linkedin: "//a[starts-with(@href, 'https://www.linkedin.com/')]/@href",
        yelplink: "//a[contains(@href, 'yelp.fr/biz')]/@href",
        emails: ["//a[starts-with(@href, 'mailto:')]/@href", {extract: /mailto:([^?)()]+)/i, remove: /mailto:|%20/ig}],
        emailtexts: ["//text()[contains(., '@') or contains(., '[at]')]", {extract: /\b([a-z0-9_.+-]+(?:@|\[at\])[a-z0-9_.-]+\.[a-z0-9.]+)\b/i, replace: {search: "[at]", replace: "@"}}],
        contactlinks: "//a[contains(@href, 'contact') or contains(@href, 'Contact') or contains(@href, 'CONTACT') or contains(., 'contact') or contains(., 'Contact')  or contains(., 'CONTACT')][not(starts-with(@href, 'mailto:') or starts-with(@href, 'javascript:') or starts-with(@href, '#'))][not(contains(@href, 'cookies'))]/@href",
        aboutlinks: "//a[contains(@href, 'about') or contains(@href, 'About') or contains(., 'about') or contains(., 'About')][not(starts-with(@href, 'mailto:') or starts-with(@href, 'javascript:'))][not(contains(@href, 'cookies') or contains(@href, 'partner') or contains(@href, 'business-case') or contains(@href, 'businesscase') or starts-with(@href, '#'))]/@href",
        legalmentionLinks: "//a[contains(., 'Mentions légales') or contains(., 'Légales') or contains(@href, 'legales') or contains(@href, 'Legales') or contains(@href, 'Légales')][not(starts-with(@href, 'mailto:') or starts-with(@href, 'javascript:'))][not(contains(@href, 'cookies'))]/@href",
        phones: ["//text()[contains(., 'Tél.') or contains(., 'Tèl.') or contains(., 'Tel.') or contains(., 'tél.') or contains(., 'tèl.') or contains(., 'tel.') or contains(., 'Tél:') or contains(., 'Tèl:') or contains(., 'Tel:') or contains(., 'tél:') or contains(., 'tèl:') or contains(., 'tel:') or contains(., 'Tél :') or contains(., 'Tèl :') or contains(., 'Tel :') or contains(., 'tél :') or contains(., 'tèl :') or contains(., 'tel :') or contains(., 'Port.') or contains(., 'Port:') or contains(., 'Port :') or contains(., 'Téléphone') or contains(., 'téléphone') or contains(., 'Telephone') or contains(., 'telephone') or contains(., 'phone') or contains(., 'Phone')][contains(., '0') or contains(., '1') or contains(., '2') or contains(., '3') or contains(., '4') or contains(., '5') or contains(., '6') or contains(., '7') or contains(., '8') or contains(., '9')][not(contains(., 'hébergé par'))]|//text()[contains(., 'Tél.') or contains(., 'Tèl.') or contains(., 'Tel.') or contains(., 'tél.') or contains(., 'tèl.') or contains(., 'tel.') or contains(., 'Tél:') or contains(., 'Tèl:') or contains(., 'Tel:') or contains(., 'tél:') or contains(., 'tèl:') or contains(., 'tel:') or contains(., 'Tél :') or contains(., 'Tèl :') or contains(., 'Tel :') or contains(., 'tél :') or contains(., 'tèl :') or contains(., 'tel :') or contains(., 'Port.') or contains(., 'Port:') or contains(., 'Port :') or contains(., 'Téléphone') or contains(., 'téléphone') or contains(., 'Telephone') or contains(., 'telephone') or contains(., 'phone') or contains(., 'Phone')]/following-sibling::text()[contains(., '0') or contains(., '1') or contains(., '2') or contains(., '3') or contains(., '4') or contains(., '5') or contains(., '6') or contains(., '7') or contains(., '8') or contains(., '9')][1][not(contains(., 'Télécopie') or contains(., 'télécopie') or contains(., 'Fax') or contains(., 'fax') or contains(., 'hébergé par'))]|//text()[contains(., 'Tél.') or contains(., 'Tèl.') or contains(., 'Tel.') or contains(., 'tél.') or contains(., 'tèl.') or contains(., 'tel.') or contains(., 'Tél:') or contains(., 'Tèl:') or contains(., 'Tel:') or contains(., 'tél:') or contains(., 'tèl:') or contains(., 'tel:') or contains(., 'Tél :') or contains(., 'Tèl :') or contains(., 'Tel :') or contains(., 'tél :') or contains(., 'tèl :') or contains(., 'tel :') or contains(., 'Port.') or contains(., 'Port:') or contains(., 'Port :') or contains(., 'Téléphone') or contains(., 'téléphone') or contains(., 'Telephone') or contains(., 'telephone') or contains(., 'phone') or contains(., 'Phone')]/following::text()[contains(., '0') or contains(., '1') or contains(., '2') or contains(., '3') or contains(., '4') or contains(., '5') or contains(., '6') or contains(., '7') or contains(., '8') or contains(., '9')][1][not(contains(., 'Télécopie') or contains(., 'télécopie') or contains(., 'Fax') or contains(., 'fax') or contains(., 'hébergé par'))]|//span[@itemprop='telephone']|//*[contains(@*, 'phone') or contains(@*, 'tel:')]/text()[contains(., '0') or contains(., '1') or contains(., '2') or contains(., '3') or contains(., '4') or contains(., '5') or contains(., '6') or contains(., '7') or contains(., '8') or contains(., '9')][1]|//*[contains(@*, 'phone') or contains(@*, 'tel:')]/following-sibling::text()[contains(., '0') or contains(., '1') or contains(., '2') or contains(., '3') or contains(., '4') or contains(., '5') or contains(., '6') or contains(., '7') or contains(., '8') or contains(., '9')][1]",
                {extractall: /(\+?\s?(?:\d\d)?[\s\.]?[0-9][0-9\s\.)()]{8,17}[0-9])/g}],
        RCS: {
            method1 : [
                "//text()[contains(., 'RCS') or contains(., 'R.C.S') or contains(., 'Rcs') or contains(., 'R.c.s') or contains(., 'rcs') or contains(., 'r.c.s') or contains(., 'Registre du Commerce') or contains(., 'Registre du commerce') or contains(., 'registre du commerce') or contains(., 'registre des commerce') or contains(., 'Siret')][not(contains(., 'hébergé par'))]",
                {extract: /(?:RCS|Registre (?:du|des) commerces?(?:\set des société)?|R\.C\.S|Siret)[^\d]+\b([A-Z]?\d+(?:(?:[0-9\s]{6,})|(?:[0-9.]{6,})|(?:[0-9-]{6,}))\d)\b/i}],
            method2: [
                "//text()[contains(., 'RCS') or contains(., 'R.C.S')][not(contains(., 'hébergé par'))]",
                {extract: /\s(\d+[0-9\s.-]+\d)\s?R\.?C\.?S\.?/i}
            ],
            method3: [
                "//text()[contains(., 'Siret') or contains(., 'siret') or contains(., 'SIRET') or contains(., 'Siren') or contains(., 'siren') or contains(., 'SIREN') or contains(., 'Numero du registre') or contains(., 'Numéro du registre') or contains(., 'RC') or contains(., 'R.C.S')][not(contains(., 'hébergé par'))]",
                {extract: /(?:(?:Sire[tn])|(?:Num[eé]ro du registre)|(?:RC)|(?:R\.C\.S))[:\s]*((?:[A-Z]\s?)?\d[\d\s.-]+\d)/i}
            ]
        },
        TVA: {
            method1: [
                "//text()[contains(., 'TVA') or contains(., 'tva')]",
                {extract: /TVA\s(?:intracommunautaire\s)?(FR[\d\s]+)\b/}
            ]
        }
    };

    var fb_xpaths = {
        facebookdata: {
            emails: xpaths.emails,
            emailtexts: xpaths.emailtexts,
            phone: ["//div[@class='_50f4'][starts-with(., 'Call ')]|//div[@class='_3-8w']/text()[starts-with(., 'Tel :')]", {extract: /^(?:Call\s|Tel\s:\s)(.+)/}]
        }
    };

    var yelp_xpaths = {
        yelpdata: {
            phone: "//span[starts-with(@class, 'biz-phone')]",
            website: ["//span[starts-with(@class, 'biz-website')]/a/@href", {extract: /url=(http[^&]+)/, decodeURI:[]}]
        }
    };

    var tmpres = {};

/*
    if(pagename==="contactpage") {

        xpaths.contactform = {
            names: "//form[@method='post']//input[not(@type='hidden')]/@name",
            inputs: {
                _wrapper_ : "//form[@method='post']//input[not(@type='hidden')]",
                "name":     "/./@name",
                "id":       "/./@id",
            }
        };
    }
*/

    this.then(function() {
        
        this.thenRemoveNodes("//script|//noscript|//style");

        this.then(function() {
            //this._jsonres = this.parseComplexData(xpaths);

            if(isfbpage)
                tmpres = this.parseComplexData(this.objcopy(fb_xpaths));
            else if(isyelppage)
                tmpres = this.parseComplexData(this.objcopy(yelp_xpaths));
            else
                tmpres = this.parseComplexData(this.objcopy(xpaths))
        });

        //this.then(function() {                this.echo("TMPRES = " + JSON.stringify(tmpres, null, 2));            });


        this.then(function() {

            for(var key in tmpres) {

                if(this._jsonres.hasOwnProperty(key)) {

                    if(Array.isArray(tmpres[key]) && Array.isArray(this._jsonres[key])) {
                        for(var i=0; i<tmpres[key].length; i++){
                            if(this._jsonres[key].indexOf(tmpres[key][i])===-1)
                                this._jsonres[key].push(tmpres[key][i]);
                        }
                    }
                }    
                else
                    this._jsonres[key] = tmpres[key];
            }
        });

        // Remove any duplicate items from lists
        this.then(function() {

            if(this._jsonres.hasOwnProperty("contactlinks"))
                this._jsonres.contactlinks = uniq_arr(this._jsonres.contactlinks, /https?:\/\/(.+[^/])\/?/i);

            if(this._jsonres.hasOwnProperty("legalmentionLinks"))
                this._jsonres.legalmentionLinks = uniq_arr(this._jsonres.legalmentionLinks, /https?:\/\/(.+[^/])\/?/i);

            if(this._jsonres.hasOwnProperty("aboutlinks"))
                this._jsonres.aboutlinks = uniq_arr(this._jsonres.aboutlinks, /https?:\/\/(.+[^/])\/?/i);
        });

        // Merge contact + legal mentions + about into contact links
        this.then(function() {
            if(this._jsonres.hasOwnProperty("contactlinks"))
                this._jsonres.contactlinks = this._jsonres.contactlinks.splice(0,3).concat(this._jsonres.legalmentionLinks.splice(0,2), this._jsonres.aboutlinks.splice(0,4));
        });

        this.then(function() {

            // Remove any duplicate items from lists
            if(this._jsonres.hasOwnProperty("phones"))
                this._jsonres.phones = uniq_arr(this._jsonres.phones);

            if(this._jsonres.hasOwnProperty("emails"))
                this._jsonres.emails = uniq_arr(this._jsonres.emails);

            if(this._jsonres.hasOwnProperty("emailtexts")) {
                //this._jsonres.emailtexts = uniq_arr(this._jsonres.emailtexts);
                this._jsonres.emails = uniq_arr(this._jsonres.emails.concat(this._jsonres.emailtexts));
                delete this._jsonres.emailtexts;
            }

            // Check that the SIREN/SIRET is not part of phone numbers array
            if(this._jsonres.hasOwnProperty("RCS") && this._jsonres.RCS.hasOwnProperty("method1")) {
                for(var k=this._jsonres.phones.length-1; k>=0; k--)
                    if(this._jsonres.phones[k] === this._jsonres.RCS.method1)
                        this._jsonres.phones.splice(k, 1);
            }

            // Check that the SIREN/SIRET is not part of phone numbers array
            if(this._jsonres.hasOwnProperty("RCS") && this._jsonres.RCS.hasOwnProperty("method2")) {
                for(var k=this._jsonres.phones.length-1; k>=0; k--)
                    if(this._jsonres.phones[k] === this._jsonres.RCS.method2)
                        this._jsonres.phones.splice(k, 1);
            }

            // Remove any duplicate items from lists
            if(this._jsonres.hasOwnProperty("contactlinks"))
                this._jsonres.contactlinks = uniq_arr(this._jsonres.contactlinks, /https?:\/\/(.+[^/])\/?/i);
        });

        this.then(function() {
            this.saveJSON(this._jsonres, this.customSearchResultsJSONs + pagename + ".json");
            this.echo("JSON results: " + JSON.stringify(this._jsonres, null, 2));
        });

    });
};

/**
 * Create a new CasperJS instance, with some default configuration parameters
 */
var casper = new Casper({
    
    logLevel: 'info', //logLevel: 'debug',

    waitTimeout: 15000,
    pageSettings: {
        webSecurityEnabled: false,
        loadImages:  false,        // The WebPage instance used by Casper will
        loadPlugins: false         // use these settings
    },

    stepTimeout: 15000,
    verbose: true,
});



casper.setSiteName("findemails");
casper.viewPortSize.width = 1140;
casper.viewPortSize.height = 2500;
casper.customWaitMultiplier = 1;
casper.initDisplayResourceRequested = false;



casper.configJSON = {

    urls : {
    },
    xpaths: {},

    tmpPath: {
        screens:  "/tmp/casperjs/findemails/screens/", 
        cookies: "/tmp/casperjs/findemails/cookies/", 
        slim_screens: "/tmp/slimerJS/findemails/screens/", 
        slim_cookies: "/tmp/slimerJS/findemails/cookies/",
        htmls:  "/tmp/casperjs/findemails/htmls/",
        slim_htmls: "/tmp/slimerJS/findemails/htmls/",
    },

    siteExclusions: [
    ],
};



//var list = casper.getObjectFromJSONFile("/tmp/casperjs/mibomedicalgroup/jsons/40509-results.json");

var siteurl = casper.cli.has("url") ? casper.cli.get("url") : "";
var sitename = casper.cli.has("name") ? casper.cli.get("name") : "home";
var list = casper.cli.has("list")? JSON.parse(casper.cli.get("list")) : [{site: siteurl, name: sitename}];

casper.customSearchResultsJSONs = casper.cli.has("jsonsout")? casper.cli.get("jsonsout") : "/tmp/casperjs/"+casper.getSiteName()+"/outjsons/";

//console.log("List: " + JSON.stringify(list, null, 2)); phantom.exit();


casper.initBrowser();

casper.eachThen(list, function(response) {

    var previousurl = null;
    var currenturl = null;

    var item = response.data;
    var index = list.indexOf(item);
    console.log("List item: " + JSON.stringify(response.data, null, 2));

    if(item.site!==null && item.site!==undefined && !/facebook\.com/i.test(item.site) && /https?:\/\//i.test(item.site)) {

        this.thenOpen(item.site, function() {
            this.echo("Opening page for site: " + item.name);
        });

        // Deal with case where there's no HTML BODY, but FRAME instead
        this.then(function() {

            if(!this.xpathExists("//html/body")) {
                console.log("NO HTML BODY");

                if(this.xpathExists("(//frame)[1]")) {

                    var framesrc = null;
                    console.log("HTML FRAME DETECTED !");

                    this.then(function() {
                        // EXTRACT FRAME SRC
                        framesrc = this.getElementAttributeByXPath("(//frame)[1]", "src");
                    });

                    this.then(function() {

                        this.thenEcho("Found frame source: " + framesrc);

                        this.thenOpen(framesrc, function() {
                            this.echo("Opening frame page: " + framesrc);
                        });
                    });
                }
            }
            
        });

        this.then(function() {
            this.parsePageData(item.name);
            previousurl = this.getCurrentPageURLFromEvaluate();
        });


        //this.then(function() {          this.echo("JSON results: " + JSON.stringify(this._jsonres, null, 2));        });
        
        this.then(function() {

            if(this._jsonres.hasOwnProperty("email") && this._jsonres.email!=="" && this._jsonres.email!==undefined && this._jsonres.email!==null && false) {

            }
            else if(this._jsonres.hasOwnProperty("contactlinks") && this._jsonres.contactlinks!=="" && this._jsonres.contactlinks!==undefined && this._jsonres.contactlinks!==null) {

                // Look in contact us / about us pages
                this.eachThen(this._jsonres.contactlinks, function(res) {

                    var contactlink = res.data;
                    var contactlinkXPath = "(//a[@href='"+res.data+"'])[1]";
                    var linkindex = this._jsonres.contactlinks.indexOf(res.data);

                    if(linkindex<=5) { // Only deal with the first 5 links

                        this.then(function() {
                            if(this.xpathExists(contactlinkXPath))
                                this.clickX(contactlinkXPath);  
                        });

                        //this.thenMouseClick("(//a[@href='"+contactlink+"'])[1]");

                        this.thenWait(1,2,1000);

                        this.then(function() {
                            currenturl = this.getCurrentPageURLFromEvaluate();
                        });

                        this.then(function() {
                            if(currenturl===previousurl && /https?:/i.test(contactlink)) {
                                
                                this.thenOpen(contactlink, function() {
                                    this.echo("Opening contact pagee: " + contactlink);
                                });

                                this.thenWait(1,2,1000);
                            }
                        });

                        this.then(function() {
                            this.parsePageData(item.name);
                        });

                        //this.thenScreen("contactpage", true);
                        //this.then(function() {                            this.echo("JSON results: " + JSON.stringify(this._jsonres, null, 2));                        });

                        this.then(function() {
                            this.back();
                        });
                    }
                });

            }
        });

        //this.then(function() {           this.saveJSON(this._jsonres, this.customSearchResultsJSONs + item.name + ".json");        });

        this.then(function() {

            if(this._jsonres.hasOwnProperty("yelplink") && this._jsonres.yelplink!==undefined && !this._jsonres.hasOwnProperty("yelpdata")) {

                this.thenOpen(this._jsonres.yelplink, function() {
                    this.echo("Opening business Yelp page: " + this._jsonres.yelplink);
                });

                this.then(function() {
                    this.parsePageData(item.name);
                });
            }


            if(this._jsonres.hasOwnProperty("facebooklink") && this._jsonres.facebooklink!==undefined  && !this._jsonres.hasOwnProperty("facebookdata")) {

                this.thenOpen(this._jsonres.facebooklink, function() {
                    this.echo("Opening business Facebook page: " + this._jsonres.facebooklink);
                });

                this.thenRemoveNode("//div[@id='pagelet_growth_expanding_cta']");

                this.thenWaitThenClick(2, 3, 1000, "//span[@class='_2yav'][.='About']/ancestor::a[1]");
	            this.thenWaitAndScreen(2, 3, 1000, "about");

                this.then(function() {
                    this.parsePageData(item.name, true);
                });
            }
        });

    }
    else if(item.site!==null && item.site!==undefined && /https:\/\/www\.facebook\.com\/.+/i.test(item.site) && /\/about\//.test(item.site)) {

        this.thenOpen(item.site, function() {
            this.echo("Opening page for site: " + item.name);
        });

        this.then(function() {
            this.parsePageData(item.name, true);
        });
    }
});



casper.execute();