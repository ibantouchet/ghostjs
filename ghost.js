/** @module Ghost */

var system = require('system');
var engineUsed = null;

// Module requirements, depends on either current engine used is casperJS or slimerJS
if(phantom.casperEngine === "slimerjs") {

	console.log("Engine : SlimerJS"); engineUsed = "slimerjs";

	//require.paths.push('./');

	randomizer = require('./randomUtils');
	system = require('system');

	/**
	 * Get a copy  of the Casper class
	 * @class
	 */
	Ghost = require('casper').Casper;

	utils = require('utils');
	x = require('casper').selectXPath;
	fs = require('fs');
	f  = require('utils').format;
  //clientutils = require('clientutils');
}
else if( /casper/.test(system.args[0]) ) { // Casperjs

	console.log("Engine : CasperJS"); engineUsed = "casperjs";
}


/**
 * @typedef {Object} ConfigJSON
 * @property {{urlKey: urlValue, anotherUrlKey: anotherUrlValue}} urls - A list of url key / url value pairs
 * @property {{xpathKey: xpathValue, anotherXpathKey: anotherXpathValue}} xpaths - A list of xpath key /  xpath value pairs
 */

/**
 * Contains the main configuration JSON object
 * @property {ConfigJSON} configJSON - A  {@link ConfigJSON} object
 * @see ConfigJSON
 */
Ghost.prototype.configJSON = {empty:'yes'};

/** 
 * @property {string} customUserAgent
 */
Ghost.prototype.customUserAgent = null;

/**
 * @property {string} customCookiesFile
 */
Ghost.prototype.customCookiesFile = null;

/**
 * @property {string} customCookies
 */
Ghost.prototype.customCookies = null;

/**
 * @property {Object} selXPath
 */
Ghost.prototype.selXPath = x;


/**
 * Object handler for randomizer functions
 * @property {Object} randomizer
 */
Ghost.prototype.randomizer = {

  getRandomInt: function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  getRandomUniqueIntegers: function (min, max, count) {

    var arr = [];

    while(arr.length < count){

      var randomnumber= Math.floor(Math.random() * (max - min + 1)) + min;
      //var randomnumber=Math.floor(Math.random() * (max - min + 1)) + min;
      var found=false;

      for(var i=0;i<arr.length;i++){

        if(arr[i]==randomnumber){
          found=true;
          break;
        }
      }

      if(!found)
        arr[arr.length]=randomnumber;
    }

    return arr;
  },

  shuffle: function (array) {
  
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
};


/**
 * Object handler for date functions
 * @property {Object} dateHandler
 */
Ghost.prototype.dateHandler = {

  getDate: function(separator, minusday) {

    separator = (separator===null || separator===undefined || separator==="")? "/" : "-";
    minusday = (minusday===null || minusday===undefined || minusday==="" || minusday !== parseInt(minusday, 10))? null : minusday;

    var today = new Date();

    if(minusday!==null)
     today.setDate(today.getDate()-minusday);

    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10)
      dd='0'+dd

    if(mm<10)
      mm='0'+mm

    return yyyy +separator+ mm +separator+ dd;
  },

  getCurrentTime: function() {
    var currentime = new Date().toLocaleTimeString().match(/(\d+:\d+:\d+)/i)[1];
    return currentime;
  },

  getCurrentDateTime: function(separator) {
    return this.getDate(separator) + " " + this.getCurrentTime();
  }
};


/**
 * Object handler for range functions
 * @property {Object} rangeHandler
 */
Ghost.prototype.rangeHandler = {
  depRange: function (start, count) {
      return Array.apply(0, Array(count))
        .map(function (element, index) { 
		      val = index + start
          return val>9? ""+val : "0"+val ;  
        });
    }
};


/**
 * @property {string[]} args
 */
Ghost.prototype.args = require('system').args;

/**
 * @property {string} engineUsed
 */
Ghost.prototype.engineUsed = engineUsed;

/**
 * @property {Object} urls
 */
Ghost.prototype.urls = {};

/**
 * @property {Object} xpaths
 */
Ghost.prototype.xpaths = {};

/**
 * @property {Object} cssSel
 */
Ghost.prototype.cssSel = {};

/**
 * @property {Object} user
 */
Ghost.prototype.user = {};

/**
 * @property {Object[]} users
 */
Ghost.prototype.users = [];


/**
 * @property {string} usersjsonfile - Contains the users json file path + name , to initialize current session's users with
 */
Ghost.prototype.usersjsonfile = null;

/**
 * @property {string} screenshotsPath - Contains the screenshots folder full path
 */
 Ghost.prototype.screenshotsPath = null;


/**
 * @property {string} cookiesPath - Contains the cookies folder full path
 */
 Ghost.prototype.cookiesPath = null;


/**
 * @property {string} htmlsPath - Contains the saved htmls files folder path
 */
Ghost.prototype.htmlsPath = null;


/**
 * @property {string} jsonsPath - Contains the saved jsons files folder path
 */
Ghost.prototype.jsonsPath = null;

/**
 * @property {string} customSearchResultsHTMLs - Contains custom search results HTML files
 */
Ghost.prototype.customSearchResultsHTMLs = null;

/**
 * @property {string} customSearchResultsJSONs - Contains custom search results JSON files
 */
Ghost.prototype.customSearchResultsJSONs = null;


/**
 * @property {string} customSearchResultsHTMLs - Contains custom search results HTML files
 */
Ghost.prototype.downloadsFolder = null;


/**
 * @property {integer} screensCount
 */
Ghost.prototype.screensCount = 0;

/**
 * @property {boolean} initFlag
 */
Ghost.prototype.initFlag = false;

/**
 * @property {string[]} siteExclusions
 */
Ghost.prototype.siteExclusions = [];

/**
 * @property {boolean} removeScreensFlag
 */
Ghost.prototype.removeScreensFlag = true;

/**
 * @property {string} screensSuffix
 */
Ghost.prototype.screensSuffix = "";


/**
 * @property {string} filesPrefix
 */
Ghost.prototype.filesPrefix = "";


/**
 * @property {boolean} isMobile
 */
Ghost.prototype.isMobile = false;

/**
 * @property {boolean} varsInitFlag
 */
Ghost.prototype.varsInitFlag = false;

/**
 * @property {string[]} filesSaved - Contains the list of files saved during current session
 */
Ghost.prototype.filesSaved = [];


/**
 * @property {string[]} jsonFilesSaved - Contains the list of JSON files saved during current session
 */
Ghost.prototype.jsonFilesSaved = [];


/**
 * @property {Array} screensdones - Store the path of each screenshot taken during current session
 */
Ghost.prototype.screensdones = [];


/**
 * @property {boolean} initOnResourceReceived - If true, then the onresourcereceived event handler will be automatically initialized
 */
Ghost.prototype.initOnResourceReceived = true;


/**
 * @property {boolean} initOnResourceRequested - If true, then the onresourcerequested event handler will be automatically initialized
 */
Ghost.prototype.initOnResourceRequested = true;


/**
 * @property {boolean} initDisplayResourceRequested - When set to true, will display the urls for resources requested
 */
Ghost.prototype.initDisplayResourceRequested=true;


/**
 * @property {integer} loopBodyAfterClickWaitTime - When in loobBody function, after clicking on button/link, wait for a specific time
 */
Ghost.prototype.loopBodyAfterClickWaitTime = 2;


/**
 * @property {integer} loopBodyBeforeClickWaitTime - When in loobBody function, before clicking on button/link, wait for a specific time
 */
Ghost.prototype.loopBodyBeforeClickWaitTime = 1;


/**
 * @property {boolean} loopBodyClickRandowWait - When in loobBody function, before/after clicking on button/link, wait for a random time
 */
Ghost.prototype.loopBodyClickRandomWait = false;


/**
 * @property {integer} loopBodyClickRandowWaitMin - When in loobBody function, before/after clicking on button/link, wait for a min random time
 */
Ghost.prototype.loopBodyClickRandomWaitMin = 2;


/**
 * @property {integer} loopBodyClickRandowWaitMax - When in loobBody function, before/after clicking on button/link, wait for a max random time
 */
Ghost.prototype.loopBodyClickRandomWaitMax = 3;

/**
 * @property {boolean} disableScreenshots - If set to true, will not save the screenshot images
 */
Ghost.prototype.disableScreenshots = false;

/**
 * @property {boolean} initWaitTime - Define the minimum amount of time (unit: seconds) to wait when opening a new page (using the init method)
 */
Ghost.prototype.initWaitTime = 2;

/**
 * @property {string} siteName - Website name
 */
Ghost.prototype.siteName = null;


/**
 * @property {integer} customWaitMultiplier
 */
Ghost.prototype.customWaitMultiplier = 1;


/**
 * @property {string} todayDateString - String containing today's date
 */
Ghost.prototype.todayDateString = null;


/**
 * @property {string} todayDateForFile - String containing today's date (file name format)
 */
Ghost.prototype.todayDateForFile = null;

/**
 * @property {string} yesterdayDateString - String containing yesterday's date
 */
Ghost.prototype.yesterdayDateString = null;


/**
 * @property {string} yesterdayDateForFile - String containing yesterday's date (file name format)
 */
Ghost.prototype.yesterdayDateForFile = null;


/**
 * @property {integer} screensquality - The screenshots image quality level to use
 */
Ghost.prototype.screensquality = 65;


/**
 * @property {Object} lastResourceReceived - Object container for the last resource received
 */
Ghost.prototype.lastResourceReceived = {};


/**
 * @property {string} lastKnownStep - String container for the last know step being processed
 */
Ghost.prototype.lastKnownStep = "init";


/**
 * Object containing the viewport size configuration
 * @property {Object} viewPortSize
 * @property {number} viewPortSize.width
 * @property {number} viewPortSize.height
 */
Ghost.prototype.viewPortSize = {
	width: null,
	height: null,
};

/**
 * List of devices allowed
 * @property {string[]} devicesAllowed
 */
Ghost.prototype.devicesAllowed = [
  "custom", 
  "pc", "firefox-windows", "win10-chrome56", "win10-chrome56-v2", "winseven-chrome56", "win10-firefox51", "winseven-firefox51",
  "linux-chrome", "linux-firefox", "linux-firefox14", "linux-chrome56",
  "mac", "mac-firefox", "macOSX-chrome", "macOSX-safari10",
  "tablet", "ipad", 
  "iphone", "mobile", "windows-phone",
];


Ghost.prototype.screensResolutions = [
  {width: 1920, height: 1080},
  {width: 1680, height: 1050},
  {width: 1600, height: 900},
  {width: 1536, height: 864},
  {width: 1440, height: 900},
  {width: 1366, height: 768},
  {width: 1280, height: 1024},
  {width: 1280, height: 800},
  {width: 1280, height: 720},
  {width: 1024, height: 768},
];

/**
 * @property {string} Current device in use
 */
Ghost.prototype.device = null;


/**
 * @property {string} Current browser in use
 */
Ghost.prototype.browser = null;


/**
 * GETTERS AND SETTERS
 */


/**
 * Turn on/off the initialization of the 'onresourcereceived' event
 * @param {boolean} init - True to turn on, false otherwise
 * @returns {Ghost} - this
 */
 Ghost.prototype.setInitOnResourceReceived = 
/** @lends Ghost */
function(init) {
	this.initOnResourceReceived = init;
	return this;
};


/**
 * Get the state of the initialization of the 'onresourcereceived' event
 * @returns {boolean} - State of the initialization of the 'onresourcereceived' event
 */
 Ghost.prototype.getInitOnResourceReceived = 
/** @lends Ghost */
function() {
	return this.initOnResourceReceived;
};



/**
 * Turn on/off the initialization of the 'onresourcerequested' event
 * @param {boolean} init - True to turn on, false otherwise
 * @returns {Ghost} - this
 */
 Ghost.prototype.setInitOnResourceRequested = 
/** @lends Ghost */
function(init) {
	this.initOnResourceRequested = init;
	return this;
};


/**
 * Get the state of the initialization of the 'onresourcerequested' event
 * @returns {boolean} - State of the initialization of the 'onresourcerequested' event
 */
 Ghost.prototype.getInitOnResourceRequested = 
/** @lends Ghost */
function() {
	return this.initOnResourceRequested;
};



/**
 * Get the screenshots files folder path
 * @returns {string} The current screenshots files folder path
 */ 
Ghost.prototype.getScreenshotsPath =
/** @lends Ghost */
function() {
	return this.screenshotsPath;
};


/**
 * Set the screenshots files folder path
 * @param {string} screenshotsPath - The screenshots files folder path to use
 * @returns {Ghost} - this
 */ 
Ghost.prototype.setScreenshotsPath =
/** @lends Ghost */
function(screenshotsPath) {
	this.screenshotsPath = screenshotsPath;
	return this;
};


/**
 * Get the current website's name
 * @returns {string} The website's name
 */
Ghost.prototype.getSiteName = 
/** @lends Ghost */
function() {
  return this.siteName;
};


/**
 * Set the current website's name
 * @param {string} name - The website's name
 */
Ghost.prototype.setSiteName = 
/** @lends Ghost */
function(name) {
  this.siteName = name;
  return this;
};


/**
 * Get the cookies files folder path
 * @returns {string} The current cookies files folder path
 */ 
Ghost.prototype.getCookiesPath =
/** @lends Ghost */
function() {
	return this.cookiesPath;
};


/**
 * Set the cookies files folder path
 * @param {string} cookiesPath - The cookies files folder path to use
 * @returns {Ghost} - this
 */ 
Ghost.prototype.setCookiesPath =
/** @lends Ghost */
function(cookiesPath) {
	this.cookiesPath = cookiesPath;
	return this;
};




/**
 * Get the saved htmls files folder path
 * @returns {string} The current saved htmls files folder path
 */ 
Ghost.prototype.getHtmlsPath =
/** @lends Ghost */
function() {
	return this.htmlsPath;
};


/**
 * Get the saved jsons files folder path
 * @returns {string} The current saved jsons files folder path
 */ 
Ghost.prototype.getJsonsPath =
/** @lends Ghost */
function() {
  return this.jsonsPath;
};



/**
 * Set the saved htmls files folder path
 * @param {string} htmlsPath - The saved htmls files folder path to use
 * @returns {Ghost} - this
 */ 
Ghost.prototype.setHtmlsPath =
/** @lends Ghost */
function(htmlsPath) {
	this.htmlsPath = htmlsPath;
	return this;
};



/**
 * Stringify and render a JSON object
 * @param {Object} what - The JSON object to display
 */
 Ghost.prototype.renderJSON =
/** @lends Ghost */
function(what) {
    return this.echo(JSON.stringify(what, null, '  '));
 };


/**
 * Define to remove, or not, previous screenshots
 * @property {boolean} flag - TRUE to remove any previous screenshots, FALSE otherwise
 * @returns {Ghost} - this
 */
 Ghost.prototype.setRemoveScreens =
/** @lends Ghost */
function(flag) {
	this.removeScreensFlag = flag;
	return this;
 };


/**
 *  Initialize current date and current date - 1 strings
 */
Ghost.prototype.initDates =
/** @lends Ghost */
function() {
  
  var d = new Date();
  var yd = new Date(d.getTime()- 24*60*60*1000);

  var currentday = d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
  var previousday = yd.getDate()  + "/" + (yd.getMonth()+1) + "/" + yd.getFullYear();

  // Set current date's strings
  this.todayDateString = currentday.replace(/^(\d)(\/.+)/g, "0$1$2").replace(/(.+\/)(\d)(\/.+)/g, "$10$2$3"); //this.todayDateString = "08/12/2016";
  this.todayDateForFile = this.todayDateString.replace(/\//g, "-");

  // Set current date's -1 day strings
  this.yesterdayDateString = previousday.replace(/^(\d)(\/.+)/g, "0$1$2").replace(/(.+\/)(\d)(\/.+)/g, "$10$2$3");
  this.yesterdayDateForFile = this.yesterdayDateString.replace(/\//g, "-");
};



/**
 * Initialize current browser's user-agent
 * @param {string} userAgent - The user-agent string to use
 * @returns {Ghost} - this
 */
 Ghost.prototype.initUserAgent =
/** @lends Ghost */
function(userAgent) {
	this.customUserAgent = userAgent;
	return this;
 };


/**
 * Test current user agent
 */
Ghost.prototype.testUserAgent =
/** @lends Ghost */
function() {
  this.init("https://html5test.com/index.html", "html5test-home");
};


/**
 * Set current user-agent: use the customUserAgent property that has been set, if any, or use a default value otherwise
 */
 Ghost.prototype.useCustomUserAgent =
/** @lends Ghost */
function() {

	if (this.customUserAgent === null) this.customUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36";
	this.userAgent(this.customUserAgent);
 };


/**
 * Set current browser's session configuration (device type, user agent, viewport size...)
 * @param {Object} browserConfig - The browser configuration to use
 */
 Ghost.prototype.setBrowserConfig =
 /** @lends Ghost */
 function (browserConfig) {

 };


/**
 * Configure a device's settings (userAgent string + viewport size)
 * @param {string} device - The device type, can be: "custom", "pc", "mac", "tablet", "ipad", "iphone", "mobile", "linux-chrome", "firefox-windows", "linux-firefox", "mac-firefox", "windows-phone"
 * @param {string} customUserAgent - The custom user agent to be used, in case the device type is "custom"
 * @param {Object} customViewport - The custom viewport to be used, in case the device type is "custom" - Default to {width: 1024, height:768}
 * @param {number} customViewport.width - The custom viewport width, in case the device type is "custom" - Default to 1024
 * @param {number} customViewport.height - The custom viewport height, in case the device type is "custom" - Default to 768
 */
Ghost.prototype.setDeviceConfig =
/** @lends Ghost */
function(device, customUserAgent, customViewport) {

	// Set customViewport default value
	if (customViewport!==undefined && customViewport!==null && typeof customViewport === 'object') {

  } 
  else 
    customViewport = {width: 1024, height:768};

	// Make sure device is allowed
  if (this.devicesAllowed.indexOf(device)!==-1) {

    this.device = device;

    console.log(" [GHOSTJS] *setDeviceConfig* - Device chosen: " + device);

    // See: https://design.google.com/devices/ , http://mydevice.io/devices/ , http://viewportsizes.com/
    switch(device) {

    	case "custom":
    		this.customUserAgent = customUserAgent;
    		this.viewPortSize.width = customViewport.width; this.viewPortSize.height =  customViewport.height;
    		break;

      case "pc": // Emulation: The default user agent, emulates Chrome browser on Windows // Chrome 45.0 (Windows 10 - 64 bit)
      	this.customUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
      	this.viewPortSize.width = 2560; this.viewPortSize.height = 1600;
      	this.browser = "Chrome/45.0.2454.93";
        break;

      case "win10-chrome56":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
        this.viewPortSize.width = 1680; this.viewPortSize.height = 1050;
        this.brower = "Chrome/56.0.2924.87";
        break;

      case "win10-chrome56-v2":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
        this.viewPortSize.width = 1680; this.viewPortSize.height = 1050;
        this.brower = "Chrome/56.0.2924.87";
        break;

      case "winseven-chrome56":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
        this.viewPortSize.width = 1680; this.viewPortSize.height = 1050;
        this.brower = "Chrome/56.0.2924.87";
        break;

      case "firefox-windows":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/48.0";
        this.viewPortSize.width = 2560; this.viewPortSize.height = 1600;
        this.browser = "Firefox/48.0";
        break;

      case "win10-firefox51":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0";
        this.viewPortSize.width = 2560; this.viewPortSize.height = 1600;
        this.browser = "Firefox/51.0";
        break;

      case "winseven-firefox33":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0";
        this.viewPortSize.width = 1140; this.viewPortSize.height = 2500;
        this.browser = "Firefox/33.0";
      break;

      case "winseven-firefox51":
        this.customUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0";
        this.viewPortSize.width = 1920; this.viewPortSize.height = 1080;
        this.browser = "Firefox/51.0";
        break;

      case "linux-chrome": //  Linux Chrome 52.0 (64 bit)
        this.customUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36";
        this.viewPortSize.width = 2560; this.viewPortSize.height = 1600;
        this.browser = "Chrome/43.0.2357.93";
        break;

      case "linux-chrome56": //  Linux Chrome 56.0 (64 bit)
        this.customUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
        this.viewPortSize.width = 1920; this.viewPortSize.height = 1080;
        this.browser = "Chrome/56.0.2924.87";
        break;

      case "linux-chrome58":
        this.useCustomUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36";
        this.viewPortSize.width = 1920; this.viewPortSize.height = 1080;
        this.browser = "Chrome/58.0.3029.81";
        break; 

      case "linux-firefox14": // Firefox 14.0.1 (Ubuntu 64 bit)
        this.customUserAgent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:14.0) Gecko/20100101 Firefox/14.0.1";
        this.viewPortSize.width = 1140; this.viewPortSize.height = 2500;
        this.browser = "Firefox/14.0.1";
        break;

      case "linux-firefox": // Firefox 51.0 (Ubuntu 64 bit)
        this.customUserAgent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0";
        this.viewPortSize.width = 1140; this.viewPortSize.height = 1800;
        this.browser = "Firefox/51.0";
        break;

      case "mac": // Emulation: Safari browser on OSX computer
        this.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A";
        this.viewPortSize.width = 2560; this.viewPortSize.height = 1440;
        this.browser = "Safari/7.0.3";
        break;

      case "macOSX-safari10": // Emulation: Safari browser on OSX computer
        this.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8";
        this.viewPortSize.width = 1680; this.viewPortSize.height = 1050;
        this.browser = "Safari/10.0.3";
        break;

      case "macOSX-chrome": // Emulation: Safari browser on OSX computer
        this.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
        this.viewPortSize.width = 1680; this.viewPortSize.height = 1050;
        this.browser = "Chrome/56.0.2924.87";
        break;

      case "mac-firefox":
        this.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0";
        this.viewPortSize.width = 1920; this.viewPortSize.height = 1080;
        this.browser = "Firefox/33.0";
        break;

      case "tablet": // Emulation: Chrome browser on Android tablet // Samsung SM-T530NU - Android 5.0.2 - Chrome 38.0
        this.customUserAgent = "Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-T530NU Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.2 Chrome/38.0.2125.102 Safari/537.36";
        this.viewPortSize.width = 800; this.viewPortSize.height = 1200;
        this.browser = "Chrome/38.0.2125.102";
        break;

      case "ipad": // Emulation: Safari browser on iOS (iPad)
        this.customUserAgent = "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";
        this.viewPortSize.width = 768; this.viewPortSize.height = 1024;
        this.browser = "Safari/6.0";
        break;

      case "mobile": // Emulation: Chrome browser on Android (mobile)
        this.customUserAgent = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";
        this.viewPortSize.width = 360; this.viewPortSize.height = 640;
        this.browser = "Chrome/18.0.1025.133";
        break;

      case "windows-phone": // Emulation IE browser on Windows phone
        this.customUserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)";
        this.viewPortSize.width = 360; this.viewPortSize.height = 640;
        this.browser = "IEMobile/10.0";
        break;

      case "iphone": // Emulation: Safari browser on iOS (iphone)
        this.customUserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";
        this.viewPortSize.width = 375; this.viewPortSize.height = 667;
        this.browser = "Safari/6.0";
        break;

    }
    console.log(" [GHOSTJS] *setDeviceConfig* - Device has been successfuly set: " + device);
  }
  else {
  	console.log(" [GHOSTJS] *setDeviceConfig* - Device provided is not valid: " + device);
  	return false;
  }

  return this;
};


Ghost.prototype.getUserFromId =
/** @lends Ghost */
function(id) {

  var userfound = null;

  this.users.forEach(function(element) {
    
    if (element.id==id) {
      //console.log(element.id);
      userfound=element;
    }
  });

  if(userfound!==null)
    console.log("USER has been found : " + JSON.stringify(userfound));
  
  return userfound;
};


/**
 * Set current user, given its id
 * @param {integer} id - The user id
 * @returns {Ghost} - this
 */
Ghost.prototype.setUserFromId = 
/** @lends Ghost */
function(id) {
  this.user = this.getUserFromId(id);
  return this;
};


/**
 * Init current browser users, given a JSON file containing these users as a JSON array
 * @param {string} file - The JSON file path + name, containing the users to use, as JSON array
 * @returns {Ghost} - this
 */
Ghost.prototype.initUsersFromFile =
/** @lends Ghost */
function(file) {

  this.users = this.getObjectFromJSONFile(file);
  console.log(" [GHOSTJS] *initUsersFromFile* - Filled current session's users array with: " + JSON.stringify(this.users));
  return this;
};


/**
 * Given a JSON file, return the corresponding JSON object
 * @param {string} file - The JSON file path + name, containing the JSON string to convert to a JSON object
 * @returns {Object} - The corresponding JSON object
 */
Ghost.prototype.getObjectFromJSONFile =
/** @lends Ghost */
function(file) {

  if (this.fileExists(file)) {

    console.log("JSON file '"+file+"' exists !");

    return require(file);
  }
  else {
    console.log("File '"+file+"' does not exist...");
    return null;
  }
};


/**
 * Initialize current object's instance properties with either: JSON configuration file parameters, command line options, or default values
 * @param {Object} configJSON - The JSON object to use as the JSON configuration
 * @param {Object} configJSON.urls - The current site's URLS to browse. Should be an object containing key/value pairs, where key is a name associated to an url, and value is this specific url string
 * @param {string} configJSON.urls.home - An example of key/value pair, home:"http://www.example.com"
 * @param {Object} configJSON.xpaths - JSON containing XPaths identifying current site's node elements. Should be an object containing key/value pairs, where key is a name associated to an xpath, and value is this specific xpath value
 * @param {string} configJSON.xpaths.submitLink - An example of key/value pair, submitLink:"//a[@id='submit']"
 */
 Ghost.prototype.initVars =
/** @lends Ghost */
function(configJSON) {

	console.log(" [GHOST] *initVars* - Initializing variables...");
	this.echoAndStringify(" [GHOST] *initVars* - Config: ", configJSON);

	this.varsInitFlag = true;
	this.user = null;
	this.configJSON = configJSON;

	// Deal with configuration files paths, if any
	this.screenshotsPath = (phantom.casperEngine == "slimerjs")? configJSON.tmpPath.slim_screens : configJSON.tmpPath.screens;
	this.cookiesPath = (phantom.casperEngine == "slimerjs")? configJSON.tmpPath.slim_cookies : configJSON.tmpPath.cookies;
	
	// Deal with standard configuration parameters
	if (configJSON.hasOwnProperty("urls")) 
    this.urls = configJSON.urls;

	if (configJSON.hasOwnProperty("xpaths")) 
    this.xpaths = configJSON.xpaths;

	if (configJSON.hasOwnProperty("cssSel")) 
    this.cssSel = configJSON.cssSel;

	if (configJSON.hasOwnProperty("siteExclusions")) 
    this.siteExclusions = configJSON.siteExclusions;


  // Deal with 'filesPrefix' COMMAND option, if any
  if(this.cli.has("filesPrefix")) {

    this.configJSON.filesPrefix = this.cli.get("filesPrefix");
  }

  if (this.configJSON.hasOwnProperty("filesPrefix")) 
    this.filesPrefix = this.configJSON.filesPrefix;


	// Deal with 'screensSuffix' COMMAND option, if any
	if(this.cli.has("screensSuffix")){

		if (!configJSON.hasOwnProperty("screensConfig"))
      this.configJSON.screensConfig = {screensSuffix: this.cli.get("screensSuffix")} ;
		else
      this.configJSON.screensConfig.screensSuffix = this.cli.get("screensSuffix");
	}

	// Deal with 'removeScreenshots' COMMAND option, if any
	if(this.cli.has("removeScreenshots")) {
		this.removeScreensFlag = this.cli.get("removeScreenshots");
		this.removeScreensFlag = (this.removeScreensFlag===true || this.removeScreensFlag===1 || this.removeScreensFlag==="1");
	}

	// Deal with screens configuration parameters, if any
	if (this.configJSON.hasOwnProperty("screensConfig")) {

		if (configJSON.screensConfig.hasOwnProperty("removeFlag")) 
      this.removeScreensFlag = this.configJSON.screensConfig.removeFlag;

		if (configJSON.screensConfig.hasOwnProperty("screensSuffix"))
      this.screensSuffix = this.configJSON.screensConfig.screensSuffix;
	}


	// Deal with 'isMobile' COMMAND option, if any
	if(this.cli.has("isMobile")) 
    this.configJSON.isMobile = this.cli.get("isMobile");

	// Deal with 'isMobile' configuration parameters, if any
	if (this.configJSON.hasOwnProperty("isMobile")) 
    this.isMobile = this.configJSON.isMobile;

  // IF a device option has been provided, set device config accordingly
  if(this.cli.has("device"))
    this.setDeviceConfig(this.cli.get("device"));
	

  if (configJSON.hasOwnProperty("users")) 
      this.users = configJSON.users;

	// Deal with the "user" option parameter, if any
	if(this.cli.has("user")){    	//console.log("TEST IS: " + JSON.parse(this.cli.get("user")).test);

		// If a user option has been provided in command line, assign it to current user for current browser session
		this.user = JSON.parse(this.cli.get("user"));

	} // If no 'user' option has been provided in command line parameters, look for it within the json config
	else
    if (configJSON.hasOwnProperty("user")) 
      this.user = configJSON.user;


  if(configJSON.hasOwnProperty("usersjsonfile"))
    this.usersjsonfile = configJSON.usersjsonfile;


  // IF a users json file has been provided for current session, use it
  if(this.usersjsonfile!==null && this.usersjsonfile!=="") {
    console.log(" [GHOSTJS] *initVars* - Users json file has been provided: " + this.usersjsonfile);
    this.initUsersFromFile(this.usersjsonfile);
  }
  else
    console.log(" [GHOSTJS] *initVars* - No users json file provided: " + this.usersjsonfile);


  if (this.cli.has("userid")) {

    console.log(" [GHOSTJS] *initVars* - Userid provided: " + this.cli.get("userid"));

    if(this.users.length>0) {
      this.setUserFromId(this.cli.get("userid"));
    }
    else
      console.log(" [GHOSTJS] *initVars* - No users object for current instance...");
  }
  else
    console.log(" [GHOSTJS] *initVars* - No userid has been provided.");
  

	if(this.user !== null) {

		this.echoAndStringify(" [GHOST] *initVars* - Using this user: ", this.user);

    if(this.user.hasOwnProperty("device"))
      if(this.user.device!=="")
        this.setDeviceConfig(this.user.device);

		// If user agent property has been defined, use it
		if (!this.isMobile && this.user.hasOwnProperty("userAgent") && this.user.userAgent !== "") 
      this.initUserAgent(this.user.userAgent);

		if (this.isMobile && this.user.hasOwnProperty("mobileUserAgent") && this.user.mobileUserAgent !== "") 
      this.initUserAgent(this.user.mobileUserAgent);

    if (this.user.hasOwnProperty("id"))
      this.cookiesPath = this.cookiesPath + "user_" + this.user.id + "_";
	}


    // Deal with login required param, if any
  if(this.cli.has("loginRequired")) {

    this.loginRequired = this.cli.get("loginRequired");

    // Convert to bool
    this.loginRequired = (this.loginRequired===true || this.loginRequired==="1" || this.loginRequired===1 || this.loginRequired==="true");
  }


	// If a custom cookies file option has been set, use it
	if (this.cli.has("cookiesFile")) 
    this.customCookiesFile = this.cli.get("cookiesFile");

	else if(configJSON.hasOwnProperty("cookiesFile")) 
    this.customCookiesFile = configJSON.cookiesFile;

	return this;
};


/**
 * Check if a specific node element does exist in the DOM
 * Mainly, it's a basic "then" wrapper for the casper "exists" method, taking an XPath query as the selector
 * @param {string} xpath - The XPath identifying the node element to look for
 * @returns {boolean} TRUE if the node element identified by the XPath does exist, FALSE otherwise
 */
Ghost.prototype.xpathExists =
/** @lends Ghost */
function(xpath) {
  this.echo(" [GHOSTJS] *xpathExists* : " + xpath);
	return this.exists(x(xpath));
};


/**
 * Check if a specific node element is visible
 * @param {string} xpath - The XPath expression identifying the node element to check for visibility
 * @returns {boolean} TRUe if the node element is visible, FALSE otherwise
 */
Ghost.prototype.isVisible =
/** @lends Ghost */
function(xpath) {
  this.echo(" [GHOSTJS] *isVisible* : " + xpath);
  return this.visible(x(xpath));
};


/**
 * Retrieves a random value of an attribute among each element matching the provided XPath selector
 * @param {string} xpath - The XPath selector identifying the nodes elements to get get the attribute value from
 * @param {string} attribute - The attribute's name
 * @returns {string} The random attribute value
 */
Ghost.prototype.getRandomElementAttributeFromXPath =
/** @lends Ghost */
function(xpath, attribute) {
  this.echo(" [GHOSTJS] *getRandomElementAttributeFromXPath* : " + xpath);
  var elementsAttribute = this.getElementsAttribute(x(xpath), attribute);
  return elementsAttribute[randomizer.getRandomInt(0, elementsAttribute.length-1)];
};

/**
 * Check if a specific file exists on local file system, given an absolute path and file name
 * Mainly, it's a basic "then" wrapper for the casper "fs.exists" method, taking a file path as parameter
 * @param {string} filepath - The file path + name identifying the file to look for existence
 * @returns {boolean} TRUE if the file exists on local file system, FALSE otherwise
 */
Ghost.prototype.fileExists =
/** @lends Ghost */
function(filepath) {
	return fs.exists(filepath);
};


/**
 * Remove any screenshot files that exist in current screenshot folder 
 * (screenshot folder path is defined my the main configuration parameters)
 */
Ghost.prototype.removeScreens =
/** @lends Ghost */
function() {

	if (fs.exists(this.screenshotsPath) && this.removeScreensFlag===true) {

		// Get a list all files in directory
		var list = fs.list(this.screenshotsPath);

		// Cycle through the list
		for(var x = 0; x < list.length; x++){

		  // Note: If you didn't end path with a slash, you need to do so here.
		    var file = this.screenshotsPath + list[x];

		    if(fs.isFile(file)){
		        // Do something

		        console.log("[GHOSTJS] File found: " + file);
		        fs.remove(file);
		        console.log("[GHOSTJS] File deleted !");
		    }
		}
	}
};


/**
 * Remove current browser's session screenshots
 */
Ghost.prototype.removeCurrentSessionScreens =
/** @lends Ghost */
function() {

  for(var x = 0; x < this.screensdones.length; x++){

    (function(self, idx) {

      if(fs.exists(self.screensdones[idx]) && fs.isFile(self.screensdones[idx])) {
        console.log("[GHOSTJS] File found: " + self.screensdones[idx]);
        fs.remove(self.screensdones[idx]);
        console.log("[GHOSTJS] File deleted !");
      }
    })(this, x);
  }
};


/**
 * Then go one step back (go back to previous browsing step)
 * Mainly, it's a basic "then" wrapper for the casper "back" method.
 */
 Ghost.prototype.thenBack =
/** @lends Ghost */
function() {

	this.then(function() {	
		this.back();
	});
 };


/**
 * Then reload current page - Mainly, it's a basic "then" wrapper for the casper "reload" method.
 */
 Ghost.prototype.thenReload =
/** @lends Ghost */
function() {

	this.then(function() {

		this.reload(function() {
	        this.echo("[GHOSTJS] *thenReload* - Page loaded again");
	    });
    });
 };


/**
 * Then select a specific option value from a select input, identified by its XPath
 *
 * @param {string} selectXPath - XPath identifying the select box input
 * @param {string} value - The option value to select
 */
 Ghost.prototype.thenSelectOption =
/** @lends Ghost */
function(selectXPath, value) {

	this.then(function() {

		var content = this.evaluate(function(selectXPath, optionXPath) {
		    
		    // Get specified select option element
		    var optionElem = __utils__.getElementByXPath(optionXPath);
    		optionElem.selected = true; // Change specified select option to selected

    		// Create 'change' html event
			var eventObject_change = document.createEvent('HTMLEvents');
			eventObject_change.initEvent('change', true, true, window, 1);

			// Create 'click' mouse event
			var eventObject_click = document.createEvent('MouseEvents');
		    eventObject_click.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			
			// Get specified select elemeent and fire two events on it: change + mouse click
			var selectElem = __utils__.getElementByXPath(selectXPath);
			selectElem.dispatchEvent(eventObject_change);
		    selectElem.dispatchEvent(eventObject_click);
		    
		    return selectElem.outerHTML;
            
		}, selectXPath, selectXPath+"/option[@value='"+ value +"' or .='"+ value +"']");
	});
 };



/**
 * Uploads the specified file to the form element associated with the css selector
 * @param {integer} minWait - Min. amount of time to wait for
 * @param {integer} maxWait - Max. amount of time to wait for
 * @param {integer} multiWait - Should be equal to 1 000 if the amount of time has to be in seconds
 * @param {string} cssSel - Selector identifying the associated form element
 * @param {string} filePath - The path and name of the file to be uploaded
 */
Ghost.prototype.thenWaitThenUploadFile =
/** @lends Ghost */
function(minWait, maxWait, multWait, cssSel, filePath) {

    // WAIT + UPLOAD file
    this.then(function(){
        // WAIT
        this.thenWait(minWait, maxWait, multWait);
        // UPLOAD file
        this.then(function(){        
          console.log(" [GHOSTJS] *thenWaitThenUploadFile*  - Uploading file: " + filePath);
          this.page.uploadFile(cssSel, filePath);
        });
    });
};



/**
 * Checks if a specific HTML files does already exist, or not
 * @param {string} targetFile - The HTML target file to be checked
 * @param {function} callback - The callback to apply once the check has been done
 */
Ghost.prototype.htmlExists = function(targetFile, callback) {

  var htmlExists = null;

  this.then(function(){

    if (this.configJSON.tmpPath.hasOwnProperty("slim_htmls") && phantom.casperEngine == "slimerjs")
      this.configJSON.tmpPath.htmls = this.configJSON.tmpPath.slim_htmls;
  });


  this.then(function(){
    if( !/\//.test(targetFile) ) targetFile = this.configJSON.tmpPath.htmls + targetFile;
    if( !/\.html$/.test(targetFile) ) targetFile = targetFile + ".html";
  });

  this.then(function(){
    htmlExists = fs.exists(fs.absolute(targetFile));
    this.echo("[ GHOSTJS ] *htmlExists* - Looking for: " + targetFile);
    this.echo("[ GHOSTJS ] *htmlExists* - Found: " + htmlExists);
  });

  this.then(function() {

    if (callback!==null && callback!==undefined) {
      this.echo("[ GHOSTJS ] *htmlExists* - Before callback: " + callback.name);
      callback.call(this, htmlExists);
    }

  });

};



/**
 * Clicks on a DOM element, using Javascript method
 * @param {string} xpath - The XPath selector identifying the node element to be clicked
 */
Ghost.prototype.thenClickJS = 
/** @lends Ghost */
function(xpath) {

  this.then(function() {

    this.evaluate(function(elemXPath, optionXPath) {

      // Get specified select option element
      var elem = __utils__.getElementByXPath(elemXPath);

      // Create 'click' mouse event
      var eventObject_click = document.createEvent('MouseEvents');
      eventObject_click.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

      elem.dispatchEvent(eventObject_click);

    }, xpath);

  });
};



/**
 * Set a DOM input value, using Javascript method
 * @param {string} xpath - The XPath selector identifying the input element to be set
 */
Ghost.prototype.thenSetInputValue = 
/** @lends Ghost */
function(xpath, value) {

  this.then(function() {

    this.evaluate(function(elemXPath, inputValue) {

      // Get specified select option element
      var elem = __utils__.getElementByXPath(elemXPath);

      elem.value = inputValue;

    }, xpath, value);

  });
};




/**
 * Sends a "mouseclick" event on a DOM element, using CasperJS' mouse.click method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseClick=
/** @lends Ghost */
function(xpath) {
  this.then(function() {
    this.echo(" [GHOSTJS] *thenMouseClick* - Sending 'mouseclick' event onto element matching: " + xpath);
    this.mouse.click(x(xpath));
  });
};



/**
 * Sends a "mousedoubleclick" event on a DOM element, using CasperJS' mouse.doubleclick method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseDoubleclick=
/** @lends Ghost */
function(xpath) {
  this.then(function() {
    this.echo(" [GHOSTJS] *thenMouseDoubleclick* - Sending 'mousedoubleclick' event onto element matching: " + xpath);
    this.mouse.doubleclick(x(xpath));
  });
};



/**
 * Sends a "mousedown" event on a DOM element, using Javascript method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseDownJS = 
/** @lends Ghost */
function(xpath) {

  this.then(function() {

    this.evaluate(function(elemXPath, optionXPath) {

      // Get specified select option element
      var elem = __utils__.getElementByXPath(elemXPath);

      // Create 'click' mouse event
      var eventObject_click = document.createEvent('MouseEvents');
      eventObject_click.initMouseEvent('mousedown', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

      elem.dispatchEvent(eventObject_click);

    }, xpath);

  });
};


/**
 * Sends a "mousedown" event on a DOM element, using CasperJS' mouse.down method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseDown =
/** @lends Ghost */
function(xpath) {
  this.then(function() {
    this.echo(" [GHOSTJS] *thenMouseDown* - Sending 'mousedown' event onto element matching: " + xpath);
    this.mouse.down(x(xpath));
  });
};



/**
 * Sends a "mousemove" event on a DOM element, using CasperJS' mouse.move method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseMove =
/** @lends Ghost */
function(xpath) {
  this.then(function() {
    this.echo(" [GHOSTJS] *thenMouseMove* - Sending 'mousemove' event onto element matching: " + xpath);
    this.mouse.move(x(xpath));
  });
};


/**
 * Sends a "mouserightclick" event on a DOM element, using CasperJS' mouse.rightclick method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseRightclick =
/** @lends Ghost */
function(xpath) {
  this.then(function() {
    this.echo(" [GHOSTJS] *thenMouseRightclick* - Sending 'mouserightclick' event onto element matching: " + xpath);
    this.mouse.rightclick(x(xpath));
  });
};



/**
 * Sends a "mouseup" event on a DOM element, using CasperJS' mouse.up method
 * @param {string} xpath - The XPath selector identifying the node element to send the event to
 */
Ghost.prototype.thenMouseUp =
/** @lends Ghost */
function(xpath) {
  this.then(function() {
    this.echo(" [GHOSTJS] *thenMouseUp* - Sending 'mouseup' event onto element matching: " + xpath);
    this.mouse.up(x(xpath));
  });
};


/**
 * Then remove a specific node, identified by its XPath
 * 
 * @param {string} nodeXPath - XPath selecctor specifying the node element to be removed
 */
Ghost.prototype.thenRemoveNode =
/** @lends Ghost */
function(nodeXPath) {

  this.then(function() {
    this.removeNode(nodeXPath);
  });

};



/**
 * Then remove a specific set of nodes, identified by an XPath
 * 
 * @param {string} nodeXPath - XPath selecctor specifying the nodes elements to be removed
 */
Ghost.prototype.thenRemoveNodes =
/** @lends Ghost */
function(nodesXPath) {

  this.then(function() {
    this.removeNodes(nodesXPath);
  });

};


/**
 * Remove a specific node, identified by its XPath
 * 
 * @param {string} nodeXPath - XPath selecctor specifying the node element to be removed
 */
Ghost.prototype.removeNode =
/** @lends Ghost */
function(nodeXPath) {

  this.echo(" [GHOSTJS] *removeNode* - Removing element matching: " + nodeXPath);

  this.evaluate(function(xpath) {

    var element = __utils__.getElementByXPath(xpath);
    element.parentNode.removeChild(element);

  }, nodeXPath);
};


/**
 * Remove a specific set of nodes, identified by an XPath
 * 
 * @param {string} nodeXPath - XPath selecctor specifying the nodes elements to be removed
 */
Ghost.prototype.removeNodes =
/** @lends Ghost */
function(nodesXPath) {

  this.evaluate(function(xpath) {

    var elements = __utils__.getElementsByXPath(xpath);

    if (elements && elements.length) {

      Array.prototype.forEach.call(elements, function _forEach(element) {
        element.parentNode.removeChild(element);
      });
    }
  }, nodesXPath);
};



Ghost.prototype.isElementInViewport =
/** @lends Ghost */
function(nodeXPath) {

  this.evaluate(function(xpath) {

    var el = __utils__.getElementByXPath(xpath);

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );

  }, nodeXPath);
};



/**
 * Similar to the CasperJS' fetcText() method, but instead, the textual contents will be concatenated with a space
 * @param {string} nodeXPath - XPath selecctor specifying the node element from which the text should be extracted
 * @param {boolean} visibleonly - Specify whether or not to keep only visible texts
 * @param {boolean} multiplenodes - Specify whether to deal with multiple nodes matching the xpath query, nor only the first matching node
 * @return {string} The text being fetched from specified node
 */
Ghost.prototype.customFetchText =
/** @lends Ghost */
function _customfetchText(nodeXPath, visibleonly, multiplenodes) {

  return this.evaluate(function(xpath, visible, multiple) {

    if(visible===null || visible===undefined)
      visible = false;

    multiple = (multiple!== true) ? false : true;

/*
    var text = '', elements = __utils__.getElementsByXPath(xpath);

    if (elements && elements.length) {
      Array.prototype.forEach.call(elements, function _forEach(element) {
        text = text + " " + element.textContent || element.innerText || element.value || '';
      });
    }
    */

    function _isVisible(e) {
      return !!( e.offsetWidth || e.offsetHeight || e.getClientRects().length );
    }

    function _isElemVisible(elem) {

        var style;

        try {
            style = window.getComputedStyle(elem, null);
        } catch (e) {
            return false;
        }

        if(style.visibility === 'hidden' || style.display === 'none') 
          return false;
        
        var cr = elem.getBoundingClientRect();
        
        return cr.width > 0 && cr.height > 0;
    }

    function getTextFromNode(node, addSpaces, shouldBeVisible) {

      var i, result, text, child;
      result = '';

      for (i = 0; i < node.childNodes.length; i++) {

        child = node.childNodes[i];
        text = null;

        if (child.nodeType === 1)
          text = getTextFromNode(child, addSpaces, shouldBeVisible);
        else
          if (child.nodeType === 3 && (!shouldBeVisible || (shouldBeVisible && _isElemVisible(node))) )
//          if (child.nodeType === 3)
            text = child.nodeValue;
        
        
        if (text && text!=="") {

          if (addSpaces && /\S$/.test(result) && /^\S/.test(text))
            text = ' ' + text;
          
          result += text;
        }
      }
      return result;
    }

    if(multiple===true) {

      var elements = __utils__.getElementsByXPath(xpath);

      return elements.map(function(e){
          return getTextFromNode(e, true, visible);
      });
    }
    else {
      var element = __utils__.getElementByXPath(xpath);
      return getTextFromNode(element, true, visible);
    }

  }, nodeXPath, visibleonly, multiplenodes);
};



/**
 * Given a string, return the number of real words count
 * @param {string} fullStr - The string from which to count the words number
 * @return {integer} The total number of words in the string
 */
Ghost.prototype.wordCount =
/** @lends Ghost */
function _wordCount(fullStr) {

    if (fullStr===null || fullStr.length === 0)
        return 0;
    else {
        fullStr = fullStr.replace(/\./g, " ");
        fullStr = fullStr.replace(/,/g, " ");
        fullStr = fullStr.replace(/-/g, " ");
        fullStr = fullStr.replace(/\s[^\sàÀ]\s/g, " ");
        fullStr = fullStr.replace(/\r+/g, " ");
        fullStr = fullStr.replace(/\n+/g, " ");
        fullStr = fullStr.replace(/[^A-Za-z0-9Àôêîûâéèààçù\/ ]+/gi, "");
        fullStr = fullStr.replace(/^\s+/, "");
        fullStr = fullStr.replace(/\s+$/, "");
        fullStr = fullStr.replace(/\s+/gi, " ");

        var splitString = fullStr.split(" ");
        return splitString.length;
    }
};


/**
 * Given a string, remove any char that is not part of a word
 * @param {string} - The string to be cleaned
 * @return {string} The cleaned string
 */
Ghost.prototype.keepWordChars =
/** @lends Ghost */
function _keepWordChars(fullStr) {

  if (fullStr===null || fullStr.length === 0 || fullStr===undefined)
    return "";

  fullStr = fullStr.replace(/\./g, " ");
  fullStr = fullStr.replace(/,/g, " ");
  fullStr = fullStr.replace(/-/g, " ");
  fullStr = fullStr.replace(/\s[^\sàÀ]\s/g, " ");
  fullStr = fullStr.replace(/\r+/g, " ");
  fullStr = fullStr.replace(/\n+/g, " ");
  fullStr = fullStr.replace(/[^A-Za-z0-9Àôêîûâéèààçù\/ ]+/gi, "");
  fullStr = fullStr.replace(/^\s+/, "");
  fullStr = fullStr.replace(/\s+$/, "");
  fullStr = fullStr.replace(/\s+/gi, " ");

  return fullStr;
};



/**
 * Given a string, remove any unclean char
 * @param {string} fullStr - The string to be cleaned
 * @param {boolean} returnsAsArray - Whether to return an array or not
 * @return {string} The cleaned string
 */
Ghost.prototype.cleanChars =
/** @lends Ghost */
function _cleanChars(fullStr, returnsAsArray) {

  if (fullStr===null || fullStr.length === 0 || fullStr===undefined)
    return returnsAsArray===true? [] : "";

  if(Array.isArray(fullStr)) {

    ghostself = this;

    return fullStr.map(function(e){
      return ghostself.cleanChars(e);
    }).filter(function(x) {                                
      return x !=="";
    });
  }

  //fullStr = fullStr.replace(/\s[^\sàÀ]\s/g, " ");
  fullStr = fullStr.replace(/\r+/g, " ");
  fullStr = fullStr.replace(/\n+/g, " ");
  //fullStr = fullStr.replace(/[^A-Za-z0-9Àôêîûâéèààçù\/ ]+/gi, "");
  fullStr = fullStr.replace(/^\s+/, "");
  fullStr = fullStr.replace(/^-+/, "");
  fullStr = fullStr.replace(/^\s+/, "");
  fullStr = fullStr.replace(/\s+$/, "");
  fullStr = fullStr.replace(/\s+/gi, " ");

  return fullStr;
};


/**
 * Copy (deep) an object, not by reference
 * @param {Object} o - The object to copy
 * @return A copy of the input object
 */
Ghost.prototype.objcopy = 
/** @lends Ghost */
function(o) {

   var out, v, key;
   out = Array.isArray(o) ? [] : {};
   for (key in o) {
       v = o[key];

       if(v instanceof RegExp || v.constructor.name==="RegExp")
        out[key] = v;
       else
        out[key] = (typeof v === "object") ? this.objcopy(v) : v;
   }
   return out;
};


/**
 * Parse complex data structure, based on a JSON mapping config
 * @param {Object} mapping - Object representing the label -> xpath mapping
 * @param {string} wrapperXPath - The XPath expression of the current object's wrapper
 * @return {Object} JSON object containing extracted data, based on the provided mapping
 */
Ghost.prototype.parseComplexData = 
/** @lends Ghost */
function _parseComplexData(mapping, wrapperXPath) {

  if(wrapperXPath===undefined || wrapperXPath===null)
    wrapperXPath="";

  var datares = {};

  // IF mapping is an object
  if(mapping === Object(mapping) && !Array.isArray(mapping)) {

    var wrapperitemscount = 0;

    if (mapping.hasOwnProperty("_wrapper_")) {

      // Append current wrapper XPath to the context existing one, if any
      wrapperXPath+= mapping._wrapper_;
      
      // Copy the mapping object in order to gelete the _wrapper_ key, without affecting the mapping oject
      var _mapping = this.objcopy(mapping);
      //delete mapping._wrapper_;
      delete _mapping._wrapper_;

      // Count the number of items wrapped
      // If current wrapper XPath contains an occurence of '[]', remove it for the items count step
      wrapperitemscount = this.getElementsCount(wrapperXPath.replace("[]", ""));

      if (wrapperitemscount) {

        datares = [];
        var indexes = [];

        // Fill in list of indexes to use
        for (i = 1; i <= wrapperitemscount; i++)
          indexes[i] = i;

        for(var index in indexes) {

          // If current wrapper XPath contains an occurence of '[]', replace it with current index: [index]
          if(/\[\]/i.test(wrapperXPath))
            datares.push(this.parseComplexData(_mapping, wrapperXPath.replace("[]", "["+index+"]")));
          else // Else simply append the index predicat condition to the end of the wrapper XPath string
            //datares.push(this.parseComplexData(_mapping, wrapperXPath + "["+index+"]"));
            datares.push(this.parseComplexData(_mapping, "("+wrapperXPath + ")["+index+"]"));
        }
      }
    }
    else {

      var rawvalues = {};

      //this.echo(" [GHOSTJS] Current context's wrapper: " + wrapperXPath);

      for(var label in mapping) {

        //this.echo(" [GHOSTJS] Dealing with label= "+label+", mapping[label] = " + JSON.stringify(mapping[label]));

        if (!mapping.hasOwnProperty(label) || label==="_wrapper_")
          continue;

        // IF dealing with subobject
        if(mapping[label] === Object(mapping[label]) && !Array.isArray(mapping[label])) {
          
          var subobj = this.parseComplexData(mapping[label], wrapperXPath);

          // IF sub object is not empty
          if(JSON.stringify(subobj) !== JSON.stringify({}))
            rawvalues[label] = datares[label] = subobj;

          // If dealing with object of type "table" 
          // exemple: table : {_wrapper_:"//div[@id='wrapper']", label: "/span[1]", data: "/span[2]"}
          if((label==="table" || label==="_table_") && Array.isArray(datares[label])) 
            if(datares[label].length)
              if(datares[label][0].hasOwnProperty("label") && datares[label][0].hasOwnProperty("data")) {
                var _tmpdata = {};
                Array.prototype.map.call(datares[label], function(e, eindex) {
			            _tmpdata[e.label] = e.data;
		            });

                if(label==="_table_") {
                  //datares = _tmpdata;
                  for(var _label in _tmpdata)
                    datares[_label] = _tmpdata[_label];
                  
                  delete datares._table_;
                }
                else
                  datares[label] = _tmpdata;
              }
          
        }
        // IF dealing with pair: Label=>XPath
        else {

          var callbacksPipe = {};
          var currentXPath = mapping[label];
          var tmpres = null;

          // IF XPath is not a string but an array containing string + callbacks
          if(Array.isArray(currentXPath)) {
            callbacksPipe = currentXPath[1];
            currentXPath = currentXPath[0];
          }


          // IF label to copy starts or ends with ":", and do not starts with "/"
          if (/^[^\/]/i.test(currentXPath) && (/^:/i.test(currentXPath) || /:$/i.test(currentXPath))) {

            var copylabel = currentXPath.replace(":", "");

            if(/^:/i.test(currentXPath)) { // IF label to copy starts with ":"
              if(rawvalues.hasOwnProperty(copylabel)) {

                tmpres = Array.isArray(rawvalues[copylabel])? rawvalues[copylabel].slice() : rawvalues[copylabel];
              }
              else
                this.echo("Could not find property '"+copylabel+"' within the 'rawvalues' object");
            }
            else if(/:$/i.test(currentXPath)) { // IF label to copy ends with ":"
              if(datares.hasOwnProperty(copylabel)) {

                tmpres = Array.isArray(datares[copylabel])? datares[copylabel].slice() : datares[copylabel];
              }
              else
                this.echo("Could not find property '"+copylabel+"' within the 'datares' object");
            }
          }
          else {

            if (wrapperXPath!=="" && /\|\//i.test(currentXPath)) {
              var xpathpartsres = currentXPath.match(/(\/[^|]+)\|(\/[^|]+)/i);
              currentXPath = "(" + wrapperXPath + xpathpartsres[1] + "|" + wrapperXPath + xpathpartsres[2] + ")[1]";
            }
            else
              currentXPath = wrapperXPath + currentXPath;


            tmpres = this.parseComplexData(currentXPath);
          }

          //this.echo(" Got data: '" + tmpres + "', for label: '" + label + "'");

          // Get a new fresh copy by cloning the array and getting a reference to this newly created array
          if(tmpres!==undefined && tmpres!==null)
            rawvalues[label] = tmpres.slice();

          //var itemres = tmpres;
          var itemres = (tmpres!==undefined)? (Array.isArray(tmpres) ? tmpres : [tmpres]) : [];


          // Deals with the callbacks pipe, applying callbacks one by one, sequentially, in order they appear
          for(var callback in callbacksPipe) {

            var callbackParams = callbacksPipe[callback];
            var callbackParam = null;
            var tmpvalues = [];

            //this.echo("Dealing with callback: '" + callback + "', for label: '" + label + "', with params: " + callbackParams.toString());

            if(callbackParams instanceof RegExp || callbackParams.constructor.name==="RegExp" || typeof callbackParams === 'string' || callbackParams instanceof String) {
              callbackParam = callbackParams;
            }
            else if (callbackParams === Object(callbackParams)) {
            }

            

            //this.echo("Callback param: " + callbackParam.toString());
            //this.echo("Callback params: " + JSON.stringify(callbackParams));

            // FOR EACH items found (matching the XPath criteria)
            for (var i = 0; i < itemres.length; i++) {

              //this.echo("Dealing with callback: '" + callback + "', for label: '" + label + "', with value: " + itemres[i]);

              switch(callback) {

                case "atob":
                  itemres[i] = atob(itemres[i]);
                  break;

                case "decodeURI":
                  itemres[i] = decodeURIComponent(itemres[i]);
                  break;

                case "remove":

                  if (callbackParam !== null && (callbackParam instanceof RegExp || callbackParam.constructor.name==="RegExp")) {
                    itemres[i] = itemres[i].replace(callbackParam, "");
                  }

                  break;

                case "replace":

                  if(callbackParams.hasOwnProperty("search") && callbackParams.hasOwnProperty("replace"))
                    itemres[i] = itemres[i].replace(callbackParams.search, callbackParams.replace);
                
                  break;

                case "extract":

                  if (callbackParam !== null && (callbackParam instanceof RegExp || callbackParam.constructor.name==="RegExp")) {

                    //this.echo("Extracting data from: " + itemres[i]);

                    //if(itemres[i]!==null)
                      rgxres = itemres[i].match(callbackParam);


                    if(rgxres !== null && rgxres!==undefined) {
                      itemres[i] = rgxres[1];
                      //this.echo("Regex res: " + JSON.stringify(rgxres));
                    }
                    else
                      itemres[i] = "";
                  }

                  break;

                case "extractall":

                  if (callbackParam !== null && (callbackParam instanceof RegExp || callbackParam.constructor.name==="RegExp")) {

                    //this.echo("Extracting data from: " + itemres[i]);

                    //if(itemres[i]!==null)
                      rgxres = itemres[i].match(callbackParam);


                    if(rgxres !== null && rgxres!==undefined) {

                      itemres[i] = rgxres[0];

                      for(var j=1; j<rgxres.length; j++)
                        if(rgxres[j]!==null)
                          tmpvalues.push(rgxres[j]);
                     
                      //this.echo("Regex res: " + JSON.stringify(rgxres));
                    }
                    else
                      itemres[i] = "";
                  }

                break;

              } // END SWITCH

              // Eventually, remove any trailing space char
              itemres[i] = itemres[i].trim();

            } // END FOR EACH ITEM

            for (var j = tmpvalues.length - 1; j >= 0; j--)
              itemres.push(tmpvalues[j]);
          }

          // Remove empty items
          for (var k = itemres.length - 1; k >= 0; k--)
            if (itemres[k] === "" || itemres[k]===null)
                itemres.splice(k, 1);


          /**
           * Assign final metadata value to corresponding label
           * - If label ends with 's' or '*', get the full array values
           * - If label does not end with 's' or '*', only retrieve the first value
           */
          datares[label.replace(/\*$/i, "")] = /[s|\*]$/i.test(label) ? itemres : itemres[0];
        }
      }
    }
  }
  else {

    if(/^_.+_$/i.test(mapping)) {
      
      var fieldlabel = mapping.match(/^_(.+)_$/i);

      switch(fieldlabel[1]) {

        case "url":
          datares = [this.getCurrentPageURLFromEvaluate()];
          break;

        case "userid":
          if(this.user.hasOwnProperty("id"))
            datares = [this.user.id];
          break;

        case "date":
          datares = [this.dateHandler.getCurrentDateTime("-")];
          break;

        case "ip":
          if(this.user.hasOwnProperty("ip"))
            datares = [this.user.ip];
          break;

        case "query":
          if(this.user.hasOwnProperty("query"))
            datares = [this.user.query];
            break;
      }
    }
    else {
      var matchres = mapping.match(/(.+)\/@([a-z-_0-9]+)$/i);
      
      if (matchres!==null && this.xpathExists(matchres[1])) {
        if(matchres[2]==="html" || matchres[2]==="xml")
          datares = this.getElementHTML(matchres[1], true);
        else
          datares = this.getElementsAttributeByXPath(matchres[1], matchres[2]);
      }
      else
        datares = this.getElementsTextValues(mapping);
    }
  }

  //console.log(" [GhostJS] *parseComplexData* with wrapper: "+wrapperXPath+", mapping: " + JSON.stringify(mapping) + ", and datares= " + JSON.stringify(datares));
  return datares;
};


/**
 * Get a specific element node's HTML code, given its XPath selector
 * @param {String} xpath - The selector identifying the element
 * @return {String} The element node's HTML code
 */
Ghost.prototype.getElementHTML =
/** @lends Ghost */
function(xpath, outer) {
  return this.getHTML(x(xpath), outer);
};


/**
 * Parse complex data structure from current page context and save resulting data to JSON file
 * @param {Object} xpathmap - The complex data structure label->xpath mapping object
 * @param {String} filename - The JSON file name to save parsed data to
 */
Ghost.prototype.parseAndSaveComplexData =
/** @lends Ghost */
function(xpathmap, filename) {

  //this.echo(" [GHOSTJS] xpathmap dump: ");
  //require('utils').dump(xpathmap);

  // Clone the mapping object, to avoid any alteration
  var mapping = this.objcopy(xpathmap);
  //var mapping = xpathmap;
  //var jsonresults = null;

  //this.then(function() {
  var jsonresults = this.parseComplexData(mapping);
  //});

  //this.then(function() {
  if (this.customSearchResultsJSONs!=="" && this.customSearchResultsJSONs!==null) {
    this.echo("Data parsed: " + JSON.stringify(jsonresults, null, 3));
    this.saveJSON(jsonresults, this.customSearchResultsJSONs + filename + ".json");
  }

  return jsonresults;
  //});
};


/**
 * Then parse complex data structure from current page context and save resulting data to JSON file
 * This is basically a wrapper to the "parseAndSaveComplexData" method
 * @param {Object} xpathmap - The complex data structure label->xpath mapping object
 * @param {String} filename - The JSON file name to save parsed data to
 */
Ghost.prototype.thenParseAndSaveComplexData =
/** @lends Ghost */
function(xpathmap, filename) {

  this.then(function() {
    this.parseAndSaveComplexData(xpathmap, filename);
  });
};



/**
 * Return current page URL , using page context
 */
Ghost.prototype.getCurrentPageURLFromEvaluate =
/** @lends Ghost */
function() {

  return this.evaluate(function() {
    return document.URL;
  });
};



/**
 * @param {string} xpathSelector - XPath selector specifying which part of the HTML document should be retrieved, if we don't want te whole HTML
 */
 Ghost.prototype.getHTMLFromEvaluate =
/** @lends Ghost */
function(xpathSelector) {

	var content = this.evaluate(function(selector) {

		if (selector===null || selector===undefined || selector==="")
			return document.documentElement.innerHTML;
		else {
			var result = document.evaluate(selector, document.documentElement, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
			return result.snapshotItem(0).innerHTML;
		}
	}, xpathSelector);

	return content;
};



/**
 * Save page markup to a target file.
 * 
 * @param {string} targetFile - A target filename
 * @param {string} selector - XPath selector specifying which HTML part should be saved, if we don't want to save the whole HTML document
 * @param {boolean} fromJavascript - Set to TRUE if the HTML should be loaded from dynamic Javascript page context
 * @return {Ghost} this
 */
 Ghost.prototype.savePageContent =
/** @lends Ghost */
function(targetFile, selector, fromJavascript) {

	var pagecontents = null;
  var pagecontentswithoutJS =  (selector===null || selector===undefined || selector==="") ? this.getPageContent() : this.getHTML(x(selector), true);
  var pagecontentswithJS = (selector===null || selector===undefined || selector==="") ? this.getHTMLFromEvaluate() : this.getHTMLFromEvaluate(selector);

  this.then(function(){
    this.echo(" [GHOSTJS] *savePageContent* - Getting page contents...");
  });

	this.then(function(){
    pagecontents = fromJavascript===true ? pagecontentswithJS : pagecontentswithoutJS;
	});


	this.then(function(){
		// If a 'slim_htmls' property exists in the tmpPath configuration, and if current engine is slimerJS
		if (this.configJSON.tmpPath.hasOwnProperty("slim_htmls") && phantom.casperEngine == "slimerjs") {
			// Assign slimer HTMLs path to current temporary HTMLs path
			this.configJSON.tmpPath.htmls = this.configJSON.tmpPath.slim_htmls;
		}
	});


	this.then(function(){

		// If current target file is not a file name with folder path (only a file name), then create full file path
		if( !/\//.test(targetFile) ) {

      targetFile = this.configJSON.tmpPath.htmls + targetFile;

      // If the current temporary HTMLs folder does not exist, then create it
      if (!fs.exists(this.configJSON.tmpPath.htmls)) 
        fs.makeTree(this.configJSON.tmpPath.htmls);
    }
    else {
      var outdir = targetFile.match(/(\/.+\/)[^\/]+/)[1];

      if (!fs.exists(outdir)) 
        fs.makeTree(outdir);
    }

		// If current target file does not end with the ".html" substring, then fix this
		if( !/\.html$/.test(targetFile) ) 
      targetFile = targetFile + ".html";
	});


	this.then(function(){

		this.echo("[GHOST] savePageContent to: " + this.configJSON.tmpPath.htmls + " - with javascript evaluate: " + fromJavascript===true? "Y":"N");

		// Get the absolute path.
	  targetFile = fs.absolute(targetFile);
	  
	  // Let other code modify the path.
	  //targetFile = this.filter('page.target_filename', targetFile) || targetFile;
	  
	  this.log(f("Saving page html to %s", targetFile), "debug");
	  
	  // Try saving the file.
	  try {
	   	fs.write(targetFile, pagecontents, 'w');
	  } catch(err) {
	   	this.log(f("Failed to save page html to %s; please check permissions (" + err + ") ", targetFile), "error");
	   	return this;
	  }

	  this.log(f("Page html saved to %s", targetFile), "info");

	  // Trigger the page.saved event.
	  this.emit('page.saved', targetFile);
  });

  this.then(function(){
		this.filesSaved.push(targetFile);
	});

  return this;
};


/**
 * Save a JSON object to a specific target file
 *
 * @param {Object} jsonObj - The JSON object to be saved
 * @param {string} targetFile - The target file where the JSON object should be saved to
 * @returns {Ghost} this
 */
Ghost.prototype.saveToJSONFile =
/** @lends Ghost */
function(jsonObj, targetFile) {

  this.then(function(){

    // If a 'slim_htmls' property exists in the tmpPath configuration, and if current engine is slimerJS
    if (this.configJSON.tmpPath.hasOwnProperty("slim_jsons") && phantom.casperEngine == "slimerjs")
      // Assign slimer JSONs path to current temporary JSONs path
      this.configJSON.tmpPath.jsons = this.configJSON.tmpPath.slim_jsons;

    // If current target file is not a file name with folder path (only a file name), then create full file path
    if( !/\//.test(targetFile) ) {

      targetFile = this.configJSON.tmpPath.jsons + (this.filesPrefix? this.filesPrefix + "_" : "") + targetFile;

      // If the current temporary JSONs folder does not exist, then create it
      if (!fs.exists(this.configJSON.tmpPath.jsons)) 
        fs.makeTree(this.configJSON.tmpPath.jsons);
    }
    else {
      var outdir = targetFile.match(/(\/.+\/)[^\/]+/)[1];

      if (!fs.exists(outdir)) 
        fs.makeTree(outdir);
    }

    // If current target file does not end with the ".json" substring, then fix this
    if( !/\.json$/.test(targetFile) ) 
      targetFile = targetFile + ".json";
  });

  this.then(function(){
    this.echo("[GHOST] saveToJSONFile " + targetFile);
    this.saveJSON(jsonObj, targetFile);
  });
};

/**
 * Save a JSON object to a specific target file
 *
 * @param {Object} jsonObj - The JSON object to be saved
 * @param {string} targetFile - The target file where the JSON object should be saved to
 * @returns {Ghost} this
 */
 Ghost.prototype.saveJSON =
/** @lends Ghost */
function(jsonObj, targetFile) {

	  // Get the absolute path.
	  targetFile = fs.absolute(targetFile);
	  
	  this.log(f("Saving JSON to %s", targetFile), "debug");
	  
	  // Try saving the file.
	  try {
	    fs.write(targetFile, JSON.stringify(jsonObj), 'w');
	    
	  } catch(err) {
	    this.log(f("Failed to save JSON to %s; please check permissions (" + err + ") ", targetFile), "error");
	    return this;
	  }

	  this.log(f("JSON saved to %s", targetFile), "info");

    this.jsonFilesSaved.push(targetFile);

	  return this;
 };


 Ghost.prototype.writeToFile =
/** @lends Ghost */
function(contents, targetFile) {

  // Get the absolute path.
  targetFile = fs.absolute(targetFile);
    
  this.log(f("About to save contents to %s", targetFile), "debug");

  // Try saving the file.
  try {
    fs.write(targetFile, contents, 'w');    
  } catch(err) {
    this.log(f("Failed to save contents to %s; please check permissions (" + err + ") ", targetFile), "error");
    return this;
  }

  this.log(f("Contents saved to %s", targetFile), "info");
};

/**
* Then, copy a source file to destination file - Wrapper to the 'copyFile' method
* @param {string} src - The source file path+name to be copied
* @param {string} dest - The destination file path + name
*/
Ghost.prototype.thenCopyFile = 
/** @lends Ghost */
function(src, dest) {

  this.then(function() {
    this.copyFile(src, dest);
  });
};



/**
* Copy a source file to destination file
* @param {string} src - The source file path+name to be copied
* @param {string} dest - The destination file path + name
*/
Ghost.prototype.copyFile =
/** @lends Ghost */
function(src, dest) {

  // Get the absolute path.
  srcFile = fs.absolute(src);
  destFile = fs.absolute(dest);

  // Make sure that both the source and dest file do exist
  if (!fs.exists(srcFile)) {
    this.log(f("Failed copying src file %s to dest %s; please check source file do exist", srcFile, destFile), "error");
  }

  // Try to copy source file to dest file
  try {

    fs.copy(srcFile, destFile);
  // Deal with any error that might occur when trying to copy file
  } catch(err) {
    this.log(f("Failed to copy src file %s to dest %s; please check permissions (" + err + ") ", srcFile, destFile), "error");
      return this;
  }

  return this;
};


/**
* Then, move a source file to destination file - Wrapper to the 'moveFile' method
* @param {string} src - The source file path+name to be moved
* @param {string} dest - The destination file path + name
*/
Ghost.prototype.thenMoveFile =
/** @lends Ghost */
function(src, dest) {

  this.then(function() {
    this.moveFile(src, dest);
  });
};


/**
* Move a source file to destination file
* @param {string} src - The source file path+name to be moved
* @param {string} dest - The destination file path + name
*/
Ghost.prototype.moveFile =
/** @lends Ghost */
function(src, dest) {

  // Get the absolute path.
  srcFile = fs.absolute(src);
  destFile = fs.absolute(dest);

  // Make sure that both the source and dest file do exist
  if (!fs.exists(srcFile) || !fs.exists(destFile)) {
    this.log(f("Failed moving src file %s to dest %s; please check source and dest files do exist", srcFile, destFile), "error");
  }

  // Try to move source file to dest file
  try {

    fs.move(srcFile, destFile);
  // Deal with any error that might occur when trying to move file
  } catch(err) {
    this.log(f("Failed to move src file %s to dest %s; please check permissions (" + err + ") ", srcFile, destFile), "error");
      return this;
  }

  return this;
};


/**
 * Download a resource to local filesystem, which URL is located in an HTML element's attribute
 * @param {string} xpath - XPath identifying the HTML element
 * @param {string} attr - Attribute of the HTML element from which to extracte the resoure URL
 * @param {string} saveTo - The file destination
 */
Ghost.prototype.thenDownloadFrom = 
/** @lends Ghost */
function(xpath, attr, saveTo) {

  var resourceurl = null;
  var ext = null;

  if(this.downloadsFolder!==null && /^\/.+/.test(this.downloadsFolder))
    if(!fs.exists(this.downloadsFolder))
      if(this.makeTree(this.downloadsFolder))
        console.log(' [GHOSTJS] *thenDownloadFrom* - Folder "'+this.downloadsFolder+'" has just been created !');


  saveTo = (!/^\/.+/.test(saveTo) && this.downloadsFolder!==null && /^\/.+/.test(this.downloadsFolder))? this.downloadsFolder + saveTo : saveTo;

  this.then(function() {
    resourceurl = this.getElementsAttributeByXPath(xpath, attr);
    ext = this.extractFromElementAttribute(xpath, attr, /[^\/]+(\.[^\.]+)$/);
  });

  this.then(function() {
    console.log(" [GHOSTJS] *thenDownloadFrom* - Resource URL is: " + resourceurl);
    console.log(" [GHOSTJS] *thenDownloadFrom* - Resource ext is: " + ext);
  });

  this.then(function() {
    this.download(resourceurl, saveTo+ext);
  });

};

/**
 * Ghost operations: THEN ECHO
 * Then echo a string - Mainly, it's a basic "then" wrapper for the casper "echo" method.
 *
 * @param {string} msg - The text to be first displayed
 */
 Ghost.prototype.thenEcho =
/** @lends Ghost */
function(msg) {

	this.then(function() {
		this.echo(msg);
	});
 };


/**
 * Ghost operations: THEN ECHO and STRINGIFY object
 * Then echo a string, concatenated with a stringified object
 *
 * @param {string} msg - The text to be first displayed
 * @param {object} object - The object to be stringified and displayed evntually
 */
 Ghost.prototype.thenEchoAndStringify =
/** @lends Ghost */
function(msg, object) {

	this.then(function() {
		this.echoAndStringify(msg, object);
	});
 };


/**
 * Echo a string, concatenated with a stringified object
 * @param {string} msg - The text to be first displayed
 * @param {object} object - The object to be stringified and displayed evntually
 */
 Ghost.prototype.echoAndStringify =
/** @lends Ghost */
function(msg, object) {

	this.echo(msg + JSON.stringify(object));
 };


/**
 * Ghost operations: THEN + SAVE PAGE CONTENT
 * Then save page content to a specific file
 * @param {string} targetFile - The target file where current page contents should be saved to
 * @param {string} selector - XPath selector specifying which HTML part should be saved, if we don't want to save the whole HTML document
 * @param {boolean} fromJavascript - Set to TRUE if the HTML should be loaded from dynamic Javascript page context
 */
Ghost.prototype.thenSavePageContent =
/** @lends Ghost */
function(targetFile, selector, fromJavascript) {

	// Save page content
	this.then(function() {
	 this.savePageContent(targetFile, selector, fromJavascript);
  });
};


/**
 * Then save JSON object to a specific file
 * @param {Object} jsonObj - The JSON object to be saved
 * @param {string} targetFile - The file where the JSON object should be saved to
 */
 Ghost.prototype.thenSaveJSON =
/** @lends Ghost */
function(jsonObj, targetFile) {

	// Save page content
	this.then(function() {
		this.saveJSON(jsonObj, targetFile);
	});
 };


/**
 * Returns the next screenshot file name (because screenshots files names include a screenshots counter)
 * @param {string} screenName - The screenshot file name
 * @returns {string} The next screenshot file name
 */
 Ghost.prototype.getNextScreenshotFile =
/** @lends Ghost */
function(screenName) {
	return ++this.screensCount + "-" + screenName + (this.screensSuffix ? "_p" + this.screensSuffix : "");
 };


/**
 * Returns the current screenshot file name (because screenshots files names include a screenshots counter)
 * @param {string} screenName - The screenshot file name
 * @returns {string} The current screenshot file name
 */
 Ghost.prototype.getCurrentScreenshotFile =
/** @lends Ghost */
function(screenName) {
	return this.screensCount + "-" + screenName + (this.screensSuffix ? "_p" + this.screensSuffix : "");
 };



/**
 * Returns the current screenshot file full path (because screenshots files names include a screenshots counter)
 * @param {string} screenName - The screenshot file name
 * @returns {string} The current screenshot file full path
 */
Ghost.prototype.getCurrentScreenshotFileFullPath =
/** @lends Ghost */
function(screenName) {
  return this.screenshotsPath + this.getCurrentScreenshotFile(screenName) +".png";
};


/**
 * Take a screenshot and save it to provided screenshot file name
 - There is no need to provide the full screenshot file path, only the name is required. The screenshot file path is determined by the configuration parameters
 * @param {string} screenName - The screenshot file name
 * @param {boolean} savePageContentsFlag - Flag that should be set to true in the case the HTML contents should be saved. False otherwise
 */
 Ghost.prototype.saveScreenshot =
/** @lends Ghost */
function(screenName, savePageContentsFlag) {

 	  this.then(function() {
      if (!this.disableScreenshots)
  		  this.echo("Saving screenshot: " + this.getNextScreenshotFile(screenName) +".png");
  	});

  	this.then(function() {
      // IF screenshots has not been disabled
      if (!this.disableScreenshots) {
  	  	// Save page screenshot
  	  	this.capture(this.screenshotsPath + this.getCurrentScreenshotFile(screenName) +".png", undefined, {
  		    format: 'png',
  	      	quality: this.screensquality
  	  	});

        this.screensdones.push(this.screenshotsPath + this.getCurrentScreenshotFile(screenName) +".png");
      }
  	});

  	this.then(function() {
		  if (savePageContentsFlag && !this.disableScreenshots)
        this.savePageContent(this.screenshotsPath + this.getCurrentScreenshotFile(screenName) + ".html");
  	});
 };


/**
 * Ghost operations: THEN + SCREEN
 * - Then take a screenshot and save it to provided screenshot file name
 * - There is no need to provide the full screenshot file path, only the name is required. The screenshot file path is determined by the configuration parameters
 * @param {string} screenName - The screenshot file name
 * @param {boolean} savePageContentsFlag - Flag that should be set to true in the case the HTML contents should be saved. False otherwise
 */
 Ghost.prototype.thenScreen =
/** @lends Ghost */
function(screenName, savePageContentsFlag) {

	// Save screenshot
	this.then(function() {
		this.saveScreenshot(screenName, savePageContentsFlag);
	});
 };


/**
 * Ghost operations: THEN + CAPTURE SELECTOR
 * - Then capture/take screenshot of a specific node element in the DOM, identified by its XPath
 * - Mainly, it's a basic "then" wrapper for the casper "captureSelector" method.
 * @param {string} xpath - The XPath identifying the node element to be captured/screenshot
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenCaptureSel =
/** @lends Ghost */
function(xpath, screenName) {

	this.screensCount++;

	this.echo(" [GHOSTJS] *thenCaptureSel* - Capturing selector: " + this.screensCount + "-" + screenName+".png");

	this.then(function(){
    this.captureSelector(this.screenshotsPath + this.screensCount + "-" + screenName+".png", x(xpath));
	});
 };


/**
 * Inject cookies into current browsing session, given a specific cookies file name (file name only, not path)
 * @param {string} cookieFilename - The file name where saved cookies should be took from
 */
 Ghost.prototype.injectFileCookies =
/** @lends Ghost */
function(cookieFilename) {

	var cookieFileFullpath = this.cookiesPath + cookieFilename;

    if (fs.exists(cookieFileFullpath)) {

        console.log("[GHOSTJS] *injectFileCookies* - Cookies file exists, at: " + cookieFileFullpath);

        var cookiesData = fs.read(cookieFileFullpath);
        phantom.cookies = JSON.parse(cookiesData);

        return this;
    }
    else {
    	console.log("[GHOSTJS] *injectFileCookies* - ! ! ! Cookies does not exist, at: " + cookieFileFullpath + " ! ! ! ");
    	return false;
    }
 };

/**
 * Inject cookies into current browsing session, given a specific cookies file (file's full path + name)
 * @param {string} cookieFile - The file where saved cookies should be took from
 */
 Ghost.prototype.injectCookiesFromFile  =
/** @lends Ghost */
function(cookieFile) {

    if (fs.exists(cookieFile)) {

        console.log("[GHOSTJS] *injectCookiesFromFile* - Cookies file exists, at: " + cookieFile);

        var cookiesData = fs.read(cookieFile);
        phantom.cookies = JSON.parse(cookiesData);

        return this;
    }
    else return false;
 };


/**
 * Determine if the path is a file. It returns a boolean, true if the path is a file.
 * @param {string} file
 * @return {boolean}
 */
Ghost.prototype.isFile =
/** @lends Ghost */
function(file) {

  return fs.isFile(file);
};



/**
 * List the contents of a directory on the file system as simple array
 * @param {string} path
 */
Ghost.prototype.listDir =
/** @lends Ghost */
function(path) {

  if(fs.isDirectory(path)) {
    console.log("Analysing dir with path: " + path);
    return fs.list(path);
  }
};


/**
 * Creates a directory at the given path
 * @param {string} path
 */
Ghost.prototype.makeDir =
/** @lends Ghost */
function(path) {
  if(fs.makeDirectory(path))
    console.log('"'+path+'" was created.');
};


/**
 * Creates all necessary directories to be able to create the directory.
 * @param {string} path
 */
Ghost.prototype.makeTree =
/** @lends Ghost */
function(path) {
  if(fs.makeTree(path))
    console.log('"'+path+'" was created.');
};


/**
 * - Save current browsing session cookies to given file name. The file path is determined by the cookiesPath configuration parameter
 * @param {string} cookieFilename - The file name where cookies should be saved to
 */
 Ghost.prototype.saveCookies =
/** @lends Ghost */
function(cookieFilename) {

	console.log("[ GHOSTJS ] *saveCookies* - Cookie saving has just been required !");

	if (this.user.hasOwnProperty("id")) {
		if (!fs.exists(this.cookiesPath.replace("user_" + this.user.id + "_", ""))) 
      fs.makeDirectory(this.cookiesPath.replace("user_" + this.user.id + "_", ""));
	}
	else if (!fs.exists(this.cookiesPath))
    fs.makeDirectory(this.cookiesPath);

	var cookiesJSONData = JSON.stringify(phantom.cookies);
	var targetFile = this.cookiesPath + cookieFilename;
	
	// Get the absolute path.
	targetFile = fs.absolute(targetFile);
	
	this.echo("Saving cookies file to: " + targetFile);

	// Remove file if already exists
	if(fs.exists(targetFile)) 
    fs.remove(targetFile);

	// Try saving the file.
	try {
		fs.write(targetFile, cookiesJSONData, 'w'); //this.echo(" *** [COOKIES] :"+ cookiesJSONData);

	} catch(err) {
		this.log(f("Failed to save cookies to %s; please check permissions (" + err + ")", targetFile), "error");
		return this;
	}
 };


/**
 * Ghost operations: THEN SAVE COOKIES
 * - Then save current session cookies to given file name
 * @param {string} cookieFilename - The file name where cookies should be saved to
 */
 Ghost.prototype.thenSaveCookies =
/** @lends Ghost */
function(cookieFilename) {

	// Save screenshot
	this.then(function() {
		this.saveCookies(cookieFilename);
	});
 };


/**
 * Ghost operations: OPEN + THEN WAIT and saveScreenshot
 * - Open a page given its URL, then wait for a specific amount of time, and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {number} waitTime - The time amount to wait for (expressed in millisecond)
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.openThenWaitAndScreen =
/** @lends Ghost */
function(url, waitTime, screenName) {

	this.open(url).then(function() {

		// SCREENSHOT: login page
		this.wait(waitTime, function(){
			this.saveScreenshot(screenName, true);
		});
	});
 };


/**
 * Ghost operations: OPEN + THEN WAIT and saveScreenshot
 * - Open a page given its URL, then wait for a specific amount of time, and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.openThenRandWaitAndScreen =
/** @lends Ghost */
function(url, minWait, maxWait, multiplyWait, screenName) {

		this.open(url).then(function() {

			// SCREENSHOT: login page
			this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait, function(){
				this.saveScreenshot(screenName, true);
			});
		});
 };


/**
 * Ghost operations: THEN OPEN + THEN WAIT and saveScreenshot
 * - Then open a page given its URL, then wait for a specific amount of time, and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {number} waitTime - The time amount to wait for (expressed in millisecond)
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenOpenThenWaitAndScreen =
/** @lends Ghost */
function(url, waitTime, screenName) {

		console.log("[GHOSTJS] *thenOpenThenWaitAndScreen* - Opening URL: " + url);

		this.thenOpen(url, function() {

			console.log("[GHOSTJS] *thenOpenThenWaitAndScreen* - URL opened!");

			// SCREENSHOT: login page
			this.wait(waitTime, function(){
				console.log("[GHOSTJS] *thenOpenThenWaitAndScreen* - I have waited 1 sec.");
				this.thenScreen(screenName, true);
			});
		});
 };


/**
 * Ghost operations: THEN OPEN + THEN WAIT and saveScreenshot
 * - Then open a page given its URL, then wait for a random time, defined by a min-max range, and take a screenshot
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {string} url - Page URL to be opened
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenOpenThenRandWaitAndScreen =
/** @lends Ghost */
function(url, minWait, maxWait, multiplyWait, screenName) {

		console.log("[GHOSTJS] *thenOpenThenRandWaitAndScreen* - Opening URL: " + url);

		this.thenOpen(url, function() {

			console.log("[GHOSTJS] *thenOpenThenRandWaitAndScreen* - URL opened!");

			// SCREENSHOT: login page
			this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait, function(){
				console.log("[GHOSTJS] *thenOpenThenRandWaitAndScreen* - I have waited 1 sec.");
				this.thenScreen(screenName, true);
			});
		});
 };


/**
 * Ghost operations: THEN OPEN + THEN WAITFORSELECTOR and saveScreenshot
 * - Then open a page given its URL, then wait for a node element to exist (identified by its XPath) in the DOM, and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {string} xpath - The XPath identifying the node element to wait for
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenOpenThenWaitForXPathAndScreen =
/** @lends Ghost */
function(url, xpath, screenName) {

		this.thenOpen(url, function() {

			console.log("[GHOSTJS] *thenOpenThenWaitForXPathAndScreen* - Opening page: " + url);
			console.log("[GHOSTJS] *thenOpenThenWaitForXPathAndScreen* - Waiting for XPath: " + xpath);

			this.thenWaitForSelectorAndScreen(xpath, "[GHOSTJS] *thenOpenThenWaitForXPathAndScreen* - I've waited for element " + xpath, screenName);
		});
 };


/**
 * Ghost operations: THEN OPEN + WAIT WHILE VISIBLE and saveScreenshot
 * - Then open a page given its URL, then wait while a node element is still visible, and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {string} xpath - The XPath identifying the node element
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenOpenThenWaitWhileVisibleAndScreen =
/** @lends Ghost */
function(url, xpath, screenName) {

		this.thenOpen(url, function() {

			console.log("[GHOSTJS] *thenOpenThenWaitWhileVisibleAndScreen* - Opening page: " + url);
			console.log("[GHOSTJS] *thenOpenThenWaitWhileVisibleAndScreen* - Wait while element is visible: " + xpath);

			this.thenWaitWhileVisibleAndScreen(xpath, "[GHOSTJS] *thenOpenThenWaitWhileVisibleAndScreen* - I've waited while element is visible: " + xpath, screenName);
		});
 };


/**
 * Ghost operations: THEN sendKeys
 * - Then send keys (send a value) to a specific input element, defined by its XPath
 * @param {string} xpath - The XPath identifying the input node element to send keys to
 * @param {string} value - The value to be sent to the input
 * @param {boolean} reset - Whether or not to reset the input value first. Set reset to true to empty the input, otherwise to false
 */
 Ghost.prototype.thenSendKeys =
/** @lends Ghost */
function(xpath, value, reset) {

	if (reset===undefined || reset===null) {reset = true;}

	this.then(function() {

    if (this.xpathExists(xpath)) {

      this.echo(" [GHOSTJS] *sendKeys* - I've sent keys to XPath: '"+xpath+"', with value: '" + value + "'.");
		  this.sendKeys(x(xpath), value,  { reset: reset} );
    }
    else
      this.echo(" [GHOSTJS] *sendKeys* - Could not find node element which XPath is : "+xpath);
	});

  return this;
 };


/**
 * Ghost operations: THEN scrollToBottom
 */
Ghost.prototype.thenScrollToBottom =
/** @lends Ghost */
function() {

	this.then(function() {
		this.scrollToBottom();
	});
 };


/**
 * Ghost operations: THEN WAIT + sendKeys
 * - Then wait for a random time, defined by a min-max range, and then send keys (send a value) to a specific input element, defined by its XPath
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} xpath - The XPath identifying the input node element to send keys to
 * @param {string} value - The value to be sent to the input
 * @param {boolean} reset - Whether or not to reset the input value first. Set reset to true to empty the input, otherwise to false
 */
 Ghost.prototype.thenWaitAndSendKeys =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, xpath, value, reset) {

		if (reset===undefined || reset===null) {reset = true;}

		this.then(function() {

			this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait, function() {
        if (this.xpathExists(xpath)) {
  				this.echo(" [GHOSTJS] *sendKeys* - I've sent keys to XPath: '"+xpath+"', with value: '" + value + "'.");
  				this.sendKeys(x(xpath), value, { reset: reset});
        }
        else
          this.echo(" [GHOSTJS] *sendKeys* - Could not find node element which XPath is : "+xpath);
			});
      
		});
 };


/**
 * Ghost operations: THEN WAIT
 * - Then wait for a random time, defined by a min-max range
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 */
 Ghost.prototype.thenWait =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait) {

		// Wait a few seconds
		this.then(function() {

			this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait, function() {

				this.echo(" I've waited for "+minWait+"-"+maxWait+" seconds.");
			}); // END wait a few seconds

		}); // END THEN
 };


/**
 * Ghost operations: THEN WAIT + THEN SCREEN
 * - Then wait for a random time, defined by a min-max range, and then take a screenshot
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} screenName - The screenshot file name
 * @param {bool} saveHTML - Specify if while screening page, the HTML should also be saved - Default to true
 */
 Ghost.prototype.thenWaitAndScreen =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, screenName, saveHTML) {

	this.thenWait(minWait, maxWait, multiplyWait);
	this.thenScreen(screenName, (saveHTML===undefined || saveHTML===null || saveHTML)? true : false );
  return this;
 };


/**
 * Ghost operations: THEN waitForSelector
 * - Then wait for a specific node element to exist (identified by its XPath)
 * @param {string} xpath - The XPath identifying the node element
 * @param {string} logMsg - A text message to be displayed as log
 */
 Ghost.prototype.thenWaitForSelector =
/** @lends Ghost */
function(xpath, logMsg) {

	this.then(function() {
    this.echo(" [GHOSTJS] *thenWaitForSelector* : " + xpath);
		this.waitForSelector(x(xpath), function() {
      this.saveScreenshot("waitedfor", true);
			this.echo(logMsg);
		});

	}); // END THEN
 };


/**
 * Ghost operations: THEN  waitForSelector + ECHO and SCREEN
 * - Then wait for a specific node element to exist (identified by its XPath), and then take a screenshot
 * @param {string} xpath - The XPath identifying the node element
 * @param {string} logMsg - A text message to be displayed as log
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenWaitForSelectorAndScreen =
/** @lends Ghost */
function(xpath, logMsg, screenName) {

	this.thenWaitForSelector(xpath, logMsg);
	this.thenScreen(screenName, true);
 };



/**
 * Ghost operation: THEN waitWhileVisible and ECHO log message
 * - Then wait while a specific node element is visible (identified by its XPath)
 * @param {string} xpath - The XPath identifying the node element
 * @param {string} logMsg - A text message to be displayed as log
 */
 Ghost.prototype.thenWaitWhileVisible =
/** @lends Ghost */
function(xpath, logMsg) {

	this.then(function() {
    if (this.isVisible(xpath))
      this.waitWhileXPathVisible(xpath, logMsg);
/*
		this.waitWhileVisible(x(xpath), function() {
			this.echo(logMsg);
		});
*/
	});
 };



 /**
 * Ghost operation: using XPath selector, waitWhileVisible and ECHO log message
 * - Wait while a specific node element is visible (identified by its XPath)
 * @param {string} xpath - The XPath identifying the node element
 * @param {string} logMsg - A text message to be displayed as log
 */
 Ghost.prototype.waitWhileXPathVisible =
/** @lends Ghost */
function(xpath, logMsg) {
  this.echo(" [GHOSTJS] *waitWhileXPathVisible* : " + xpath);
  this.waitWhileVisible(x(xpath), function() {
    this.echo(logMsg);
  });
 };


/**
 * @todo Complete this method implementation and documentation
 */
 Ghost.prototype.thenClickOnItemWhileExists =
/** @lends Ghost */
function(nextXPath, limit) {

	function loopBody(index, nextXPath, indexLimit) {

	    if (!this.xpathExists(nextXPath)) {
	        return;
	    }

	    if (indexLimit!==null && indexLimit!==undefined) {
	    	if (index>indexLimit) return;
	    }

	    // Scroll queried element into view
	    this.thenScrollIntoView(nextXPath);

	    this.then(function(){
	    	this.wait(1000);
	    });

	    this.then(function(){
	        // do something useful
	        this.clickX(nextXPath);
	        this.echo(" [GHOSTJS] *loopBody* - Loop index: " + index);
	    });

	    this.then(function() {
	    	this.wait(6000);
	    });

      // Screen current loop iteration
      this.thenScreen("loop-page-" + index, true);

	    this.then(function(){
	        loopBody.call(this, index+1, nextXPath, indexLimit);
	    });
	}

	this.then(function() {
		loopBody.call(this, 1, nextXPath, limit);
	});
 };



/**
 * @todo Complete this method implementation and documentation
 */
 Ghost.prototype.thenClickOnItemWhileExistsAndSavePage =
/** @lends Ghost */
function(nextXPath, pagename, limit) {

	function loopBody(index, nextXPath, pagename, indexLimit) {

	    if (!this.xpathExists(nextXPath)) {
        this.echo(" [GHOSTJS] *thenClickOnItemWhileExistsAndSavePage* loopBody - Next item XPath does not exist: " + nextXPath);
	      return;
	    }

	    if (indexLimit!==null && indexLimit!==undefined) {
	    	if (index>indexLimit) return;
	    }

	    // Scroll queried element into view
	    this.thenScrollIntoView(nextXPath);


	    this.then(function(){

        if (this.loopBodyClickRandomWait)
          this.wait(1000 * randomizer.getRandomInt(this.loopBodyClickRandomWaitMin, this.loopBodyClickRandomWaitMax));
        else
	    	  this.wait(1000 * this.loopBodyBeforeClickWaitTime);
	    });

	    this.then(function(){
	        // Click on next page button
	        this.clickX(nextXPath);
	        this.echo(" [GHOSTJS] *thenClickOnItemWhileExistsAndSavePage* loopBody - Loop index: " + index);
	    });

	    // Wait for page to be loaded
	    this.then(function() {
        if (this.loopBodyClickRandomWait)
          this.wait(1000 * randomizer.getRandomInt(this.loopBodyClickRandomWaitMin, this.loopBodyClickRandomWaitMax));
        else
	    	  this.wait(1000 * this.loopBodyAfterClickWaitTime);
	    });

	    // Screen current loop iteration
	    this.thenScreen("loop-page-" + index, true);

	    // Save current page loop iteration
      if (!this.fileExists(pagename + "_" + index + ".html"))
        this.thenSavePageContent(pagename + "_" + index);

      this.thenEcho(" [GHOSTJS] *thenClickOnItemWhileExistsAndSavePage* - About to enter newt loopBody iteration..." + (index+1));

	    this.then(function(){
	        loopBody.call(this, index+1, nextXPath, pagename, indexLimit);
	    });
	}

  if (!this.fileExists(pagename + "_0" + ".html"))
    this.thenSavePageContent(pagename + "_0");

  this.thenEcho(" [GHOSTJS] *thenClickOnItemWhileExistsAndSavePage* - About to enter first loopBody iteration...");

	this.then(function() {
		loopBody.call(this, 1, nextXPath, pagename, limit);
	});
 };



/**
 * @todo Complete this method implementation and documentation
 */
 Ghost.prototype.thenClickOnItemWhileExistsWaitingForLoader =
/** @lends Ghost */
function(nextXPath, loaderXPath, limit) {

	function loopBody(index, nextXPath, loaderXPath, indexLimit) {

	    if (!this.xpathExists(nextXPath)) {
	    	this.wait(2000);
	        return;
	    }

	    if (indexLimit!==null && indexLimit!==undefined) {
	    	if (index>indexLimit) return;
	    }

	    // Scroll queried element into view
	    this.thenScrollIntoView(nextXPath, false);

	    // Wait a second
	    this.then(function(){
	    	this.wait(1000);
	    });

	    // Click on next specified element
	    this.then(function(){
	        this.click(x(nextXPath));
	        this.echo(" [GHOSTJS] *loopBody* - Loop index: " + index);
	    });

	    // Wait a second
	    this.then(function(){
	    	this.wait(1000);
	    });

	    // Wait while loader element is still visible
	    this.thenWaitWhileVisible(loaderXPath, " [GHOSTJS] *thenClickOnItemWhileExists* - Loader no longer visible: " + loaderXPath);

	    // Wait another second
	    this.then(function(){
	    	this.wait(1000);
	    });

	    // Run another loop instance
	    this.then(function(){
	        loopBody.call(this, index+1, nextXPath, loaderXPath, indexLimit);
	    });
	}

	this.then(function() {
		loopBody.call(this, 1, nextXPath, loaderXPath, limit);
	});
 };

/**
 * Ghost operations: THEN + waitWhileVisible + THEN SCREEN
 * - Then wait while a specific node element is visible (identified by its XPath), and then take a screenshot
 * @param {string} xpath - The XPath identifying the node element
 * @param {string} logMsg - A text message to be displayed as log
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.thenWaitWhileVisibleAndScreen =
/** @lends Ghost */
function(xpath, logMsg, screenName) {

	this.thenWaitWhileVisible(xpath, logMsg);
	this.thenScreen(screenName, true);
 };


/**
 * Ghost operations: THEN WAIT + THEN CLICK + THEN SCREEN
 * - Wait for a random time, defined by a min-max range, then click on a specific node element identified by its XPath, and take a screenshot
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} xpath - The XPath identifying the node element to click on
 * @param {string} screenName - The screenshot file name
 * @param {bool} onlywhenitemexists - Only process this step if the node identified by the XPath does exist
 */
 Ghost.prototype.thenWaitThenClickAndScreen =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, xpath, screenName, onlywhenitemexists) {

  onlywhenitemexists = (onlywhenitemexists===null || onlywhenitemexists===undefined || !onlywhenitemexists)? false: true;

  this.then(function() {
    if((this.xpathExists(xpath) && onlywhenitemexists) || !onlywhenitemexists ) {
      this.thenWaitThenClick(minWait, maxWait, multiplyWait, xpath);
      this.thenWaitAndScreen(1, 2, 1000, screenName);
    }
  });
 };


/**
 * Ghost operations: THEN WAIT + THEN CLICK + THEN WAIT AND SCREEN
 * - Wait for a random time, defined by a min-max range, then click on a specific node element identified by its XPath, and then wait again for a random time and take a screenshot
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} xpath - The XPath identifying the node element to click on
 * @param {string} screenName - The screenshot file name
 * @param {bool} onlywhenitemexists - Only process this step if the node identified by the XPath does exist
 */
 Ghost.prototype.thenWaitThenClickThenWaitAndScreen =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, xpath, screenName, onlywhenitemexists) {

  onlywhenitemexists = (onlywhenitemexists===null || onlywhenitemexists===undefined || !onlywhenitemexists)? false: true;

  this.then(function() {
    if((this.xpathExists(xpath) && onlywhenitemexists) || !onlywhenitemexists ) {
      this.thenWaitThenClick(minWait, maxWait, multiplyWait, xpath);
	    this.thenWaitAndScreen(minWait, maxWait, multiplyWait, screenName);
    }
  });
 };



 /**
 * Ghost operations: THEN WAIT + THEN CLICK + THEN WAIT AND SCREEN
 * - Wait for a random time, defined by a min-max range, then click on a specific node element identified by its XPath, and then wait again for a random time and take a screenshot
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} xpath - The XPath identifying the node element to click on
 * @param {string} screenName - The screenshot file name
 * @param {bool} onlywhenitemexists - Only process this step if the node identified by the XPath does exist
 */
 Ghost.prototype.thenWaitThenClickJSThenWaitAndScreen =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, xpath, screenName, onlywhenitemexists) {

  onlywhenitemexists = (onlywhenitemexists===null || onlywhenitemexists===undefined || !onlywhenitemexists)? false: true;

  this.then(function() {
    if((this.xpathExists(xpath) && onlywhenitemexists) || !onlywhenitemexists ) {
      this.thenWaitThenClickJS(minWait, maxWait, multiplyWait, xpath);
      this.thenWaitAndScreen(minWait, maxWait, multiplyWait, screenName);
    }
  });
 };


/**
 * Ghost operations: THEN WAIT + BACK
 * - Wait for a random time, defined by a min-max range, and then go back to previous browser step
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 */
 Ghost.prototype.thenWaitAndBack =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait) {

	this.then(function() {	

		this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait, function() {
			this.back();
		});
	});
 };



/**
 * Focus on a specific DOM element node
 * @param {string} xpath - The XPath expression identifying the DOM element node that should have the focus
 */
Ghost.prototype.focusOn = 
/** @lends Ghost */
function(xpath) {

  this.evaluate(function(xpath) {

      var elems = __utils__.getElementsByXPath(xpath);
      elems[0].focus();

  }, xpath);
};



/**
 * Then, focus on a specific DOM element node
 * @param {string} xpath - The XPath expression identifying the DOM element node that should have the focus
 */
Ghost.prototype.thenFocusOn = 
/** @lends Ghost */
function(xpath) {

  this.thenEvaluate(function(xpath) {

      var elems = __utils__.getElementsByXPath(xpath);
      elems[0].focus();

  }, xpath);
};



/**
 * Ghost operations: CLICK, with XPath selector
 * Click on a specific node element, identified by its XPath
 * @param {string} xpath - The XPath identifying the node element to click on
 */
 Ghost.prototype.clickX = 
/** @lends Ghost */
function(xpath) {
  this.echo(" [GHOSTJS] *clickX* : " + xpath);
	this.click(x(xpath), function(){
		this.echo(" [GHOSTJS] *clickX* - I've clicked on node element: " + xpath);
	});
 };



/**
 * Ghost operations: THEN CLICK, with XPath selector
 * Then click on a specific node element, identified by its XPath
 * @param {string} xpath - The XPath identifying the node element to click on
 */
 Ghost.prototype.thenClickX = 
/** @lends Ghost */
function(xpath) {

  this.thenFocusOn(xpath);
  this.echo(" [GHOSTJS] *thenClickX* : " + xpath);
	this.thenClick(x(xpath), function(){
		this.echo(" [GHOSTJS] *thenClickX* - I've clicked on node element: " + xpath);
	});
  return this;
 };



 Ghost.prototype.clickOnLinkThenSavePageContentsThenBack =
 /** @lends Ghost */
 function(id, htmlfile, linkxpath, callback, callbackparams, turnoffBack) {

    this.echo(" [GHOSTJS] *thenClickOnLinkThenSavePageContentsThenBack* - Checking existence of file: " + htmlfile);

    this.htmlExists(htmlfile, function _htmlExists(htmlExists) {

      if (!htmlExists) {

        this.then(function() {
          this.echo(" [GHOSTJS] *thenClickOnLinkThenSavePageContentsThenBack* - About to click on item: " + id);
        });

        // Click on profile page link and open as popup
        //this.thenWaitThenClick(3, 5, 1000, linkxpath);

        this.thenWaitThenClickThenWaitAndScreen(3, 5, 1000, linkxpath, "clicked-on-itemlink-" + id);

        this.then(function() {
          this.wait(1000* this.customWaitMultiplier, function() {
            this.savePageContent(htmlfile);
          });
        });

        this.then(function() {
          if (callback!==null && callback!==undefined) {
            this.echo("[ GHOSTJS ] *clickOnLinkThenSavePageContentsThenBack* - Before callback: " + callback.name);
            callback.call(this, callbackparams);
          }
        });

        this.then(function() {

          if (this.xpaths.hasOwnProperty("contents"))
            this.parseAndSaveComplexData(this.xpaths.contents, "data_" + id);
        });

        // Back to main results page
        this.then(function() {
          if(!turnoffBack) {
            this.wait(2000* this.customWaitMultiplier, function() {
              this.back();
            });
          }
        });

        // Back to main results page
        this.then(function() {
          this.wait(2000* this.customWaitMultiplier, function() {

          });
        });
      }
      else 
        this.echo(" [GHOSTJS] *thenClickOnLinkThenSavePageContentsThenBack*  - HTML file already exists: " + htmlfile);
    }); // END IF HTML FILE ALREADY EXISTS
};




 Ghost.prototype.openLinkThenSavePageContents =
 /** @lends Ghost */
 function(id, htmlfile, linkurl, waitUntilXPath) {

    this.htmlExists(htmlfile, function(htmlExists) {

      if (!htmlExists) {

        this.then(function() {
          this.echo(" [GHOSTJS] *thenClickOnLinkThenSavePageContentsThenBack* - About to open link for item: " + id);
        });

        // Open link page and save page contents
        this.then(function() {
          this.wait(2000, function() {
            this.open(linkurl).then(function() {

              if (waitUntilXPath!==null && waitUntilXPath!==undefined) {

                this.waitWhileVisible(x(waitUntilXPath), function() {
                  this.wait(1000, function() {
                    this.savePageContent(htmlfile);
                  });
                });
              }
              else {
                this.wait(2500, function() {
                  this.savePageContent(htmlfile);
                }); 
              }
            });
          });
        });

        // Wait a second
        this.then(function() {
          this.wait(1000, function() {
          });
        });
      }
      else 
        this.echo(" [GHOSTJS] *thenClickOnLinkThenSavePageContentsThenBack*  - HTML file already exists: " + htmlfile);
    }); // END IF HTML FILE ALREADY EXISTS
};



/**
 * Ghost operations: THEN WAIT + THEN CLICK
 * - Wait for a random time, defined by a min-max range, and then click on a specific element, identified by its XPath
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} xpath - The XPath identifying the node element to click on
 */
 Ghost.prototype.thenWaitThenClick =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, xpath) {

	this.then(function() {

    // Only wait and click on element if it exists within the DOM
    if(this.xpathExists(xpath)) {

      this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait * this.customWaitMultiplier, function() {

        this.thenEcho(" [GHOSTJS] *thenWaitThenClick* - I've waited for "+minWait* this.customWaitMultiplier+"-"+maxWait* this.customWaitMultiplier+" seconds.");
        this.thenClickX(xpath);
      }); // END wait
    } // END if xpath exists
	});
 };



/**
 * Ghost operations: THEN WAIT + THEN CLICK
 * - Wait for a random time, defined by a min-max range, and then click on a specific element, identified by its XPath
 * - The random time range (min and max values) is expressed in millisecond, the 'multiplyWait' parameter should be used as the range multiplier, example: for a random 2-3 seconds range, minWait=2, maxWait=3 and multiplyWait=1000
 * @param {number} minWait - The random time range min value (expressed in millisecond)
 * @param {number} maxWait - The random time range max value (expressed in millisecond)
 * @param {number} multiplyWait - The random time range multiplier value (should be 1000 if the time range is expressed in seconds)
 * @param {string} xpath - The XPath identifying the node element to click on
 */
 Ghost.prototype.thenWaitThenClickJS =
/** @lends Ghost */
function(minWait, maxWait, multiplyWait, xpath) {

  this.then(function() {  

    // Only wait and click on element if it exists within the DOM
    if(this.xpathExists(xpath)) {

      this.wait(randomizer.getRandomInt(minWait, maxWait) * multiplyWait* this.customWaitMultiplier, function() {

        this.thenEcho(" [GHOSTJS] *thenWaitThenClickJS* - I've waited for "+minWait* this.customWaitMultiplier+"-"+maxWait* this.customWaitMultiplier+" seconds.");
        this.thenClickJS(xpath);
        this.thenEcho(" [GHOSTJS] *thenWaitThenClickJS* - I've clicked on button/link: " + xpath);
      }); // END wait
    } // END if xpath exists
  });
 };


/**
 * Then, write a line to STDOUT
 * @param {string} msg - The message to send to STDOUT
 */
Ghost.prototype.thenWriteLineToStdout =
/** @lends Ghost */
function(msg) {

  this.then(function(){
        system.stdout.writeLine(msg);
  });
};


/**
 * Given an XPath and an attribute name, retrieve the first matching node element's attribute value
 * @param {string} elemXPath - The XPath used to identiy the matching node
 * @param {string} attr - The attribute to retrieve
 * @returns {text} The matching node element's attribute value
 */
Ghost.prototype.getElementAttributeByXPath =
/** @lends Ghost */
function(elemXPath, attr) {
	return this.getElementAttribute(x(elemXPath), attr);
};


/**
 * Given an XPath, an attribute name and a regex, retrieve the first matching node's attribute value, and extract part matching the regex
 * @param {string} elemXPath - The XPath used to identiy the matching node
 * @param {string} attr - The attribute to retrieve
 * @param {RegExp} regexp - Regular expression to match against
 * @returns {text} The matching node's attribute value
 */
Ghost.prototype.extractFromElementAttribute =
/** @lends Ghost */
function(elemXPath, attr, regexp) {
  var matches = this.getElementAttributeByXPath(elemXPath, attr).match(regexp);
  return Array.isArray(matches)? matches[1].trim() : "";
};


/**
 * Given an XPath and an attribute name, retrieve all matching nodes elements' attributes values
 * @param {string} elemXPath - The XPath used to identify the matching nodes
 * @param {string} attr - The attribute to retrieve
 * @returns {array} An array containing all matching nodes element's attributes values
 */
Ghost.prototype.getElementsAttributeByXPath =
/** @lends Ghost */
function(elemXPath, attr) {
	return this.getElementsAttribute(x(elemXPath), attr);
};


/**
 * Given an XPath query identifying a specific set of nodes, count the number of matched elements
 * @param {string} xpath - The nodes elements' XPath
 * @returns {integer} The number of matched elements
 */
Ghost.prototype.getElementsCount = 
/** @lends Ghost */
function(xpath) {

    return this.evaluate(function(xpath) {

      var elementsCount=0, elements = __utils__.getElementsByXPath(xpath);

      if (elements && elements.length)
        elementsCount = elements.length;

      return elementsCount;

  }, xpath);
};

/**
 * Retrieves a specific node element's text, identified by given XPath
 * @param {string} xpath - The node element's XPath
 * @returns {string} The node element's text value
 */
Ghost.prototype.getElementText =
/** @lends Ghost */
function(xpath) {
  this.echo(" [GHOSTJS] *getElementText* : " + xpath);
	return this.fetchText(x(xpath)).replace(/^\s+|\s+$|\n/gm,'').replace(/\s+/g, ' ');
};


Ghost.prototype.getElementTextAndExtract =
/** @lends Ghost */
function(xpath, regexp) {

  var matches = this.getElementText(xpath).match(regexp);
  return Array.isArray(matches)? matches[1].trim() : "";
};



/**
 * Retrieves specific nodes elements' text values, identified by given XPath
 * @param {string} xpath - The nodes elements' XPath
 * @returns {array} The nodes elements' text values
 */
Ghost.prototype.getElementsTextValues =
/** @lends Ghost */
function(xpath) {

  return this.evaluate(function(xpath) {

    var textEls = [], elements = __utils__.getElementsByXPath(xpath);

    if (elements && elements.length) {

      Array.prototype.forEach.call(elements, function _forEach(element) {
        //textEls.push(element.textContent.replace(/^\s+|\s+$|\n/gm,'') || element.innerText.replace(/^\s+|\s+$|\n/gm,''));
        elemcontent =  element.textContent || element.innerText;
        textEls.push(elemcontent.replace(/^\s+|\s+$|\n/gm,'').replace(/\s+/g, ' '));
      });
    }

    return textEls;

  }, xpath);
};


/**
 * Retrieves specific nodes elements' text values, identified by given XPath, and concatenate them
 * @param {string} xpath - The node element's XPath
 * @returns {string} The node element's text value
 */
Ghost.prototype.getElementsTextsConcat =
/** @lends Ghost */
function(xpath, separator) {
  //return this.fetchText(x(xpath)).replace(/^\s+|\s+$|\n/gm,'');

  return this.evaluate(function(xpath, separator) {

    if (!separator)
      separator = '';

    var textEls = [], elements = __utils__.getElementsByXPath(xpath);

    if (elements && elements.length) {

      Array.prototype.forEach.call(elements, function _forEach(element) {
        //textEls.push(element.textContent.replace(/^\s+|\s+$|\n/gm,'') || element.innerText.replace(/^\s+|\s+$|\n/gm,''));
        elemcontent =  element.textContent || element.innerText;
        textEls.push(elemcontent.replace(/^\s+|\s+$|\n/gm,''));
      });
    }

    return textEls.join(separator);

    //return __utils__.fetchText(__utils__.makeSelector(selector, 'xpath'), separator).replace(/^\s+|\s+$|\n/gm,'');
  }, xpath, separator);
};


/**
 *
 * @param {string} xpathTemplate
 * @returns {string}
 */
Ghost.prototype.getElementsTextByXPathPattern =
/** @lends Ghost */
function(xpathTemplate) { //var xpathTemplate="//td[@class='user-info']/a[@href='%s']/div/text()";

  var elementsText = [];
  var xpathParts = /(.+)\[\@([a-z-_]+)='\%s'\]/ig.exec(xpathTemplate);

  if (xpathParts!==null) {

    var attributesFound = this.getElementsAttributeByXPath(xpathParts[1], xpathParts[2]);
    
    this.eachThen(attributesFound, function(response) {
      elementsText.push(this.getElementText(xpathTemplate.replace(/\%s/gi, response.data)));
    });
  }
  else return FALSE;

  return elementsText;
};



/**
 * 
 * @param {string} domainname
 * @params {boolean} includesubdomains
 * @returns array
 */
Ghost.prototype.getExternalLinks =
/** @lends Ghost */
function(domainname, excludesubdomains) {

  var links = [];
  excludesubdomains= (excludesubdomains===undefined || excludesubdomains===null) ? true : false;

  var patt = new RegExp("https?:\/\/[^\/]*" + domainname.replace(".", "\\."), "i");
  var xpath = "//a[starts-with(@href, 'http:') or starts-with(@href, 'https:') or starts-with(@href, 'www.')][not(starts-with(@href, 'http://"+domainname+"') or starts-with(@href, 'https://"+domainname+"') or starts-with(@href, 'https://www."+domainname+"') or starts-with(@href, 'http://www."+domainname+"') or starts-with(@href, 'www."+domainname+"') or starts-with(@href, '"+domainname+"'))]";
  
  links = this.getElementsAttributeByXPath(xpath, "href");

  if(excludesubdomains) {

    index = links.length-1;

    while(index>=0) {

      if (patt.test(links[index]))
        links.splice(index, 1);

      index-=1;
    }
  }

  return links;
};


Ghost.prototype.getUniqueValues = 

function(arr){

  var n = {},r=[];

  if(!Array.isArray(arr))
    return [];
  else {
    for(var i = 0; i < arr.length; i++)  {

      if (!n[arr[i]])    {
        n[arr[i]] = true; 
        r.push(arr[i]); 
      }
    }
    return r;
  }
};


/**
 * 
 * @param {string} domainname
 * @params {boolean} includesubdomains
 * @returns array
 */
Ghost.prototype.getInternalLinks =
/** @lends Ghost */
function(domainname, includesubdomains) {

  var links = [];
  includesubdomains= (includesubdomains===undefined || includesubdomains===null) ? true : false;

  var xpath = "//a[not(starts-with(@href, 'javascript') or starts-with(@href, '#') or starts-with(@href, 'http'))]|//a[starts-with(@href, 'http://"+domainname+"') or starts-with(@href, 'https://"+domainname+"') or starts-with(@href, 'https://www."+domainname+"') or starts-with(@href, 'http://www."+domainname+"') or starts-with(@href, 'www."+domainname+"') or starts-with(@href, '"+domainname+"')]";
  links = this.getElementsAttributeByXPath(xpath, "href");

  var subdomains = [];
  if (includesubdomains)
    subdomains = this.getSubdomainsLinks(domainname);

  return links.concat(subdomains);
};


/**
 * 
 * @param {string} domainname
 * @returns array
 */
Ghost.prototype.getSubdomainsLinks =
/** @lends Ghost */
function(domainname) {

  var links = [];

  var patt = new RegExp("https?:\/\/[^\/]*" + domainname.replace(".", "\\."), "i");
  var xpath = "//a[starts-with(@href, 'http:') or starts-with(@href, 'https:') or starts-with(@href, 'www.')][not(starts-with(@href, 'http://"+domainname+"') or starts-with(@href, 'https://"+domainname+"') or starts-with(@href, 'https://www."+domainname+"') or starts-with(@href, 'http://www."+domainname+"') or starts-with(@href, 'www."+domainname+"') or starts-with(@href, '"+domainname+"'))]";
  
  links = this.getElementsAttributeByXPath(xpath, "href");

    
  index = links.length-1;

  while(index>=0) {

    if (!patt.test(links[index]))
      links.splice(index, 1);

    index-=1;
  }

  return links;
};


/**
 * 
 */
Ghost.prototype.thenProcessSearchResults =
/** @lends Ghost */
function(searchResultNextPageLinkXPath, resultItemLinkXPath, resultItemLinkRegex, resumeToPage) {

  this.then(function() {

    this.echo(" [GHOSTJS] *thenProcessSearchResults*");
    
    htmlFilename = "results_fromsearch";

    function loopResults(pageindex, nextXPath, htmlfile) {

      var resultPageLinksList = [];

      // IF not the first loop iteration, click on next element in order to load new page content
      this.then(function() {
        if (pageindex > 1)
          this.thenWaitThenClick(2,4,1000, nextXPath);
      });

      // Screen current page
      this.thenWaitAndScreen(2, 4, 1000, "page-results" + (pageindex + resumeToPage-1) );

      // Display step processing
      this.thenEcho("[GHOSTJS] *thenProcessSearchResults* loopResults - Dealing with page: " + (pageindex + resumeToPage-1));

      // Set pages links to open as popups: add "target" attribute and set to "_blank"
      this.thenSetElementsAttribute(resultItemLinkXPath, "target", "_blank");
      
      // GET list of pages links
      this.then(function(){
        resultPageLinksList = this.getElementsAttributeByXPath(resultItemLinkXPath, "href");
      });

      // DISPLAY list of pages links
      this.then(function(){
        this.echoAndStringify(" [GHOSTJS] *thenProcessSearchResults* loopResults - Profile page links list: ", resultPageLinksList);
      });

      // THEN, DEAL WITH EACH ITEM PAGE, one by one
      this.then(function() {

        this.eachThen(resultPageLinksList, function(response) {

          var currentItemHTMLFile = null;

          var currentProfileHREF = response.data;
          var matches = currentProfileHREF.match(resultItemLinkRegex);

          var currentItemLinkXPath = resultItemLinkXPath + "[@href='" + currentProfileHREF + "']";

          this.then(function() {
            currentItemHTMLFile = htmlfile  + "_ID-" + matches[1] + ".html";
          });

          // THEN open item page, if page HTML has not already been saved
          this.then(function() {

            //if(!this.fileExists(currentItemHTMLFile)) {

            this.htmlExists(currentItemHTMLFile, function(htmlExists) {

              if (!htmlExists) {

                this.then(function() {
                  this.echo(" [GHOSTJS] *thenProcessSearchResults* loopResults - About to click on item: " + matches[1]);
                });

                // Click on profile page link and open as popup
                this.thenWaitThenClick(4, 8, 1000, currentItemLinkXPath);

                this.then(function() {

                  // Wait for profile page to be opened as popup
                  this.waitForPopup(resultItemLinkRegex, function() {
                    this.echo(" [GHOSTJS] *thenProcessSearchResults* loopResults - I've waited for the item page to be opened and loaded !");

                  }, function() {
                    this.echo("[GHOSTJS] *thenProcessSearchResults* waitForPopup - Timeout reached !");
                    // do something
                  }, 25000);

                  // Wait for profile page popup to by opened and loaded
                  this.withPopup(resultItemLinkRegex, function() {

                    this.echo(" [GHOSTJS] *thenProcessSearchResults* loopResults - I'm in the page popup context...:" + matches[1]);
                    this.savePageContent(currentItemHTMLFile);
                  });
                });

                // Close previously opened popup, see: https://groups.google.com/forum/#!topic/slimerjs/yfshI1Vfg1E
                this.then(function() {
                  this.wait(1500, function() {

                    if(phantom.casperEngine === "slimerjs") {
                      this.page.pages[0].evaluate(function(){
                        window.close();
                      });
                    }
                    else {

                      //@see: http://stackoverflow.com/questions/29547900/how-to-open-different-popups-with-the-same-title-in-casperjs
                      this.popups.length = 0;
                      this.popups.clean();
                      this.page.pages = [];
                    }
                  });
                });

                // Back to main results page
                this.then(function() {
                  this.wait(1500, function() {
                    this.echo(" [GHOSTJS] *thenProcessSearchResults* loopResults - I'm back to normal page processing");
                  });
                });
              }
              else 
                this.echo(" [GHOSTJS] *thenProcessSearchResults* loopResults - HTML file already exists: " + currentItemHTMLFile);

            });

            //}   else        this.echo(" [GHOSTJS] *thenProcessSearchResults* loopResults - HTML file already exists: " + currentItemHTMLFile);

          }); // END IF HTML FILE ALREADY EXISTS

        }); // END EACH THEN

      }); // END THEN, DEAL WITH EACH ITEM PAGE, one by one

      // Run another loop iteration, if next button exists
      this.then(function(){

          if (this.xpathExists(nextXPath))
              loopResults.call(this, pageindex+1, nextXPath, htmlfile);
      });

    } // END LOOP BODY

      // Wait a few seconds and screen
    this.thenWaitAndScreen(2, 4, 1000, "processSearchResults");

    // IF IN RESUME MODE, go to page num X
    this.then(function() {
      if (resumeToPage>1) {
        //this.thenWaitThenClick(2,4,1000, "//a[@class='page-link'][@data-page-number='"+resumeToPage+"']");
      }
    });

    // Loop through each result pages and process invidividual profile page result as popup
    this.then(function() {
      loopResults.call(this, 1, searchResultNextPageLinkXPath, htmlFilename);
    });
  });
};







/**
 * Given an XPath, scroll into view
 * @param {string} xpath - XPath query identifying the DOM element where the view should be scrolled to
 * @param {boolean} alignWithTop - Optional boolean value which, if is true, align the top of the element with the top of the visibile area of the scrollable ancestor,
 *                                 but if false, align the botton of the element to the bottom of the visibile area of the scrollable ancestor
 */
Ghost.prototype.scrollIntoView = 
/** @lends Ghost */
function(xpath, alignWithTop) {

	this.evaluate(function(elemXPath, alignWithTop) {

		var elem = __utils__.getElementByXPath(elemXPath);

		elem.scrollIntoView(alignWithTop);

	}, xpath, alignWithTop);
};



/**
 * Then, given an XPath, scroll into view
 * @param {string} xpath - XPath query identifying the DOM element where the view should be scrolled to
 * @param {boolean} alignWithTop - Optional boolean value which, if is true, align the top of the element with the top of the visibile area of the scrollable ancestor,
 *                                 but if false, align the botton of the element to the bottom of the visibile area of the scrollable ancestor
 */
Ghost.prototype.thenScrollIntoView = 
/** @lends Ghost */
function(xpath, alignWithTop) {

	this.then(function() {
		this.scrollIntoView(xpath, alignWithTop);
		this.echo(" [GHOSTJS] thenScrollIntoView - Pages scrolled to element: " + xpath);
	});
};


/**
 * Set an attribute's new value for a specific set of DOM nodes elements, identified by given XPath
 * @param {string} xpath - The XPath query identifying the elements to set the new attribute to
 * @param {string} attr - The name of the attribute to be set
 * @param {string} value - The value of the attribute to be set
 */
Ghost.prototype.setElementsAttribute = 
/** @lends Ghost */
function(xpath, attr, value) {

	this.evaluate(function(xpath, attr, value) {

		var items = __utils__.getElementsByXPath(xpath);

		// Set elements' new attribute and assign given value
		Array.prototype.map.call(items, function(e, rank) {
			e.setAttribute(attr, value); 
		});

	}, xpath, attr, value );
};


/**
 * Ghost operations: THEN + setElementsAttribute
 * Then set an attribute's new value for a specific set of DOM nodes elements, identified by given XPath
 * @param {string} xpath - The XPath query identifying the elements to set the new attribute to
 * @param {string} attr - The name of the attribute to be set
 * @param {string} value - The value of the attribute to be set
 */
Ghost.prototype.thenSetElementsAttribute = 
/** @lends Ghost */
function(xpath, attr, value) {

	this.then(function() {
		this.setElementsAttribute(xpath, attr, value);
	});

	this.then(function() {
		this.echo(" [GHOSTJS] *thenSetElementsAttribute* - Attribute '" + attr +"' has been set to elements matching query '"+ xpath +"' with value: " + value);
	});
};


/**
 * THEN + remove specific elements' attributes
 * @param {string} xpath - XPath query identifying the DOM elemnent(s) for which the attribute should be removed
 * @param {string} attr - The attribute to be removed from DOM element(s)
 */
Ghost.prototype.thenRemoveElementsAttribute =
/** @lends Ghost */
function(xpath, attr) {

	this.then(function() {

		this.evaluate(function(xpath, attr) {

			var items = __utils__.getElementsByXPath(xpath);

			// Set elements' new attribute and assign given value
			Array.prototype.map.call(items, function(e, rank) {
				e.removeAttribute(attr);
			});

		}, xpath, attr);
	});
};



/**
 * Open a page requiring the user to be authenticated first
 * @param {string} page_url - The page's URL to be opened, that requires user authentication
 */
Ghost.prototype.openPageWithLogin = function(page_url) {

  this.lastKnownStep = "openPageWithLoginStart";
  var cookieFilename = ((this.getSiteName()===null)? "site" : this.getSiteName()) + "_logged.txt";

  this.initVars(this.configJSON);

  // IF cookies file exists, no login step is required, but INIT browser instead
  if (this.injectFileCookies(cookieFilename) !== false) {   // Inject existing cookies
      
    console.log(" [GHOSTJS] *openPageWithLogin* - INIT browser and load first page...");
    this.lastKnownStep = "openPageWithLoginStartWithCookies";

    // INIT browser
    this.init(page_url, "first_page_with-cookies");
  }
  else { // IF no cookies file exists, then need to login


    if (!this.urls.hasOwnProperty("account_login"))
      this.urls.account_login = {home: page_url};

    console.log('Cookies file: "'+ this.cookiesPath + cookieFilename+'" does not exist (yet).');
    console.log(" [GHOSTJS] *openPageWithLogin* - Loading login page...");

    // INIT browser + LOGIN
    this.processLogin(this.urls.account_login.home);

    this.then(function() {
      if (this.urls.hasOwnProperty("account_login"))
        if (page_url !== this.urls.account_login.home)
          this.thenOpenThenWaitAndScreen(page_url, 3500, "first_page_after_login");
    });
  }
  
  // IF cookies are no more valid, re log-in
  this.then(function() {

    if(this.xpaths.hasOwnProperty("beforeLoginClickOn")) {

      if (this.xpathExists(this.xpaths.beforeLoginClickOn)) {
        // INIT browser + LOGIN
        this.processLogin(null);
      }
    }

    this.lastKnownStep = "openPageWithLoginEnd";
  });
};



/**
 * @param {string} url
 * @param {string} xpaths
 * @param {string} user
 */
Ghost.prototype.processLogin = 
/** @lends Ghost */
function(url, xpaths, user) {

  console.log("[GhostJS] *login* - I'am going to initialize the web browser for Login process...");
  this.lastKnownStep = "processLoginStart";

  // Init browser user agent + view port size and open home page + wait + screen
  this.init(url, "homeLogin");

	if (this.xpaths.loginEmailXPath!==undefined && this.xpaths.loginEmailXPath!==null && this.xpaths.loginPasswdXPath!==undefined && this.xpaths.loginPasswdXPath!==null && xpaths===undefined && user===undefined) {

		user = this.user;
		xpaths = this.xpaths;
	}

    console.log("User: " + JSON.stringify(this.user));
  	console.log("XPaths: " + JSON.stringify(this.xpaths));

    this.then(function() {
      this.wait(2500, function() {
      });
    });

    // If a link/button should be clicked before reaching the login frame
    this.then(function() {

      if (xpaths.hasOwnProperty("beforeLoginClickOn")) {
        // Wait + click on the login button
        this.clickX(xpaths.beforeLoginClickOn);
      }
    });
    
    // Fill the email field 
    this.thenWaitAndSendKeys(2, 4, 1000, xpaths.loginEmailXPath, user.login);

    // Wait + screen
    this.thenWaitAndScreen(2, 3, 500, "email_input_filled");

    // If a link/button should be clicked after email has been filled, and before filling password
    this.then(function() {

      if (xpaths.hasOwnProperty("loginNextButtonXPath")) {
        // Wait + click on the login button
        this.clickX(xpaths.loginNextButtonXPath);
      }
    });


    // If password input should be clicked before sending the password string
    this.then(function() {

      if (xpaths.hasOwnProperty("mousedownOnPasswordXPath") && this.xpathExists(xpaths.mousedownOnPasswordXPath)) {

        // Wait + click on the login button
        //this.clickX(xpaths.loginNextButtonXPath);
        this.thenMouseDown(xpaths.mousedownOnPasswordXPath);

        // Wait + screen
        this.thenWaitAndScreen(2, 3, 500, "mouse_down_password_input");
      }
    });



    // Fill the password field
    this.thenWaitAndSendKeys(2, 4, 1000, xpaths.loginPasswdXPath, user.password);

    // Wait + screen
    this.thenWaitAndScreen(2, 3, 500, "password_input_filled");

    // If there is a checkbox to be clicked for persistent login
    this.then(function() {

      if (xpaths.hasOwnProperty("loginPersistentXPath")) {
        // Wait + click on the login button
        this.clickX(xpaths.loginPersistentXPath);
      }
    });

    // Wait + screen
    this.thenWaitAndScreen(2, 3, 500, "Login_fields_filled");

    this.then(function() {
      this.lastKnownStep = "processLoginBeforeSubmit";
    });

    // Wait + click on the login button
    this.thenWaitThenClick(2, 3, 1000, xpaths.loginButtonXPath);

    // In case where login submit failed, try something
    this.then(function() {

      if(this.lastKnownStep==="processLoginTimedOut" && this.lastResourceReceived.hasOwnProperty("url")) {
        this.thenOpen(this.lastResourceReceived.url, function() {
          this.echo(" [GHOSTJS] *processLogin* - Opening last ressource received's url: " + this.lastResourceReceived.url);
        });
      }
    });

    this.then(function() {
      this.lastKnownStep = "processLoginAfterSubmit";
    });

    // Wait + screen
    this.thenWaitAndScreen(2, 3, 500, "Login_submit_clicked");

    this.then(function() {

      if (xpaths.hasOwnProperty("logoutButtonXPath")) {
        // Wait for the login process to be completed
        this.thenWaitForSelectorAndScreen(xpaths.logoutButtonXPath, "I've waited for the 'Log out button to be detected", "Login_successful");
      }
    });



    // If password input should be clicked before sending the password string
    this.then(function() {

      if (xpaths.hasOwnProperty("recoveryEmailConfirmation") && this.xpathExists(xpaths.recoveryEmailConfirmation)) {

        // Fill the password field
        this.thenWaitAndSendKeys(2, 4, 1000, xpaths.recoveryEmailInput, user.recoveryEmail);

        // Wait + screen
        this.thenWaitAndScreen(2, 3, 500, "recovery-email_input_filled");

        // Wait + click on the login button
        this.thenWaitThenClick(2, 3, 1000, xpaths.recoveryNextButton);

        // Wait + screen
        this.thenWaitAndScreen(4, 6, 500, "Recovery-email-confirmation-done_clicked");
      }
      else if(xpaths.hasOwnProperty("recoveryOptionsCheck") && this.xpathExists(xpaths.recoveryOptionsCheck)) {
        
        // Wait + click on the login button
        this.thenWaitThenClick(2, 3, 1000, xpaths.recoveryOptionsCheck);
        
        // Wait + screen
        this.thenWaitAndScreen(4, 6, 500, "Recovery-email-check-done_clicked");
      }
    });

    // Casper operations: THEN + ECHO + SAVE COOKIES
    this.thenSaveCookies( ((this.getSiteName()===null)? "site" : this.getSiteName()) + "_logged.txt");

    this.then(function() {
      this.lastKnownStep = "processLoginEnd";
    });
};



/**
 * Initialize the "on event" specific handlers
 */
 Ghost.prototype.initOnEvents =
/** @lends Ghost */
function() {


  this.on('http.status.403', function(resource) {

    this.then(function(){
      this.thenWaitAndScreen(2,3, 1000, "after_403_detected");
    });

    this.then(function(){
        this.echo("[WARNING - Bot detected]: "+resource.url + ' is 403');
        phantom.exit();
      });

  });
		 
	this.on('error', function(msg,backtrace) {
	  this.echo("=========================");
	  this.echo("ERROR:");
	  this.echo(msg);
	  this.echo(backtrace);
	  this.echo("=========================");
	});


	this.on("page.error", function(msg, backtrace) {

/*
    if (msg != "TypeError: undefined is not a constructor (evaluating 'b[a].innerHTML.endsWith('You still can save the leads if you click on the icon in your browser.')')") {
	   this.echo("=========================");
	   this.echo(" [ PAGE.ERROR ] : ");
     this.echo(msg);
	   this.echo(backtrace);
	   this.echo("=========================");
    }
    */

    var msgStack = ['ERROR: ' + msg];
    if (backtrace && backtrace.length) {
        msgStack.push('TRACE:');
        backtrace.forEach(function(t) {
            msgStack.push(" -> " + t.file + ": " + t.line + (t.function? " (in function '" +t.function +"')" : ""));
        });
    }
    // uncomment to log into the console 
    //console.error(msgStack.join('\n'));

	});


	this.on("load.failed", function(msg) {
	  this.echo("=========================");
	  this.echo(" [ LOAD.FAILED ] : ");
	  this.echo(msg);
	  this.echo("=========================");
	});


	if (this.initOnResourceReceived) {

		this.on("page.resource.received", function(response){
			console.log(" * [ Resource received ] : "+JSON.stringify(response));
      this.lastResourceReceived = response;
		});
	}

	this.on('remote.message', function(msg) { this.echo('[Remote message] : '+msg);});


	if(this.initOnResourceRequested) {
		// Handle resources requested events
		this.on('resource.requested', function(requestData, request) {

			//console.log(" * [ Resource requested (request)] : "+JSON.stringify(request));

			if(new RegExp(this.siteExclusions.join("|")).test(requestData.url) && this.siteExclusions.length!==0) {
				console.log("[ABORTING request]" + requestData.url);
				request.abort();

			}
			else
        if(this.initDisplayResourceRequested)
          console.log(" * [ Resource requested (url)] : "+requestData.url);
		});
	}
  else {

    this.on('resource.requested', function(requestData, request) {
      if(this.initDisplayResourceRequested)
        console.log(" * [ Resource requested (url)] : "+requestData.url);
    });
  }
 };


/**
 * Run the casperJS steps, and exit properly
 */
 Ghost.prototype.execute =
/** @lends Ghost */
function() {

	this.run(function() { 	
		phantom.exit();  
	});
 };


/**
 * Initialize the browser:
 * - Init. main configuration parameters, set screenshots, cookies, and htmls paths.
 * - Remove previous screenshots, unless configured otherwise
 * - Init. the "on event" handlers
 * - Inject specific cookies, if configured
 * - Set the custom user-agent to be used, if any configured
 * - Start the casper browser
 * - Set the viewport size
 */
 Ghost.prototype.initBrowser =
/** @lends Ghost */
function() {
		
	// If the browser has not been initialized yet
	if (this.initFlag !== true) {

    this.initDates();

		// If there is no json configuration file provided for current session, create a default one
		if (this.configJSON.hasOwnProperty("empty")) {

			// Init vars with default config
			this.initVars({
				urls : {}, 
				xpaths : {},
				user: {}, 
				tmpPath: {
					screens:  this.getScreenshotsPath() || "/tmp/casperjs/tests/screens/", 
					cookies: this.getCookiesPath() || "/tmp/casperjs/tests/cookies/", 
					slim_screens: this.getScreenshotsPath() || "/tmp/slimerJS/tests/screens/", 
					slim_cookies: this.getCookiesPath() || "/tmp/slimerJS/tests/cookies/",
					htmls:  this.getHtmlsPath() || "/tmp/casperjs/tests/htmls/",
					slim_htmls: this.getHtmlsPath() || "/tmp/slimerJS/tests/htmls/",
          jsons:  this.getJsonsPath() || "/tmp/casperjs/tests/jsons/",
          slim_jsons: this.getJsonsPath() || "/tmp/slimerJS/tests/jsons/",
				},

				siteExclusions:[
					//"google\.com",
          "fbstatic",
          "googleads",
          //"googletagmanager\.com", 
          //"google-analytics\.com",
          //"doubleclick\.net", 
          "bluekai\.com",
          "idsync\.rlcdn",
          "rubiconproject",
          "weborama\.fr",
          //"radar\.cedexis",
          "secure\.adnxs",
          "collector\.schibsted",
          "scorecardresearch\.com",
				],
			});
		}
		else if(!this.varsInitFlag) { // If the browser variables have not been initialized yet, do it.
			this.initVars(this.configJSON);
		}

	  // Remove any previous screens
	  this.removeScreens();

	  // Init casper events handlers
	  this.initOnEvents();

	  // If a custom cookies file has been set, inject it into browser
	  if (this.customCookiesFile!==null) {
	  	console.log("[GHOST] *initBrowser* - Custom cookies file has been set, injecting custom cookies...");
	   	this.injectCookiesFromFile(this.customCookiesFile);
	  }

	  // Retrieve the user agent to be used, if any
	  this.useCustomUserAgent();
	  this.echo("[Param] : User-agent used = "+ this.customUserAgent);

	  // Start browser
	  this.start();

/*
    var childPage;

    this.page.onPageCreated = function (newPage) {

      childPage = newPage;

      childPage.onLoadStarted = function () {

        var currentUrl = page.evaluate(function() {
            return window.location.href;
        });

        console.log('Start loading child page: ' + currentUrl);
      };

      childPage.onLoadFinished = function () {
        console.log('Finished loading child page');
      };

      childPage.onClosing = function(closingPage) {
        console.log('Child page is closing: ' + closingPage.url);
      };
    };
*/

	  // Set viewport size
	  if (this.viewPortSize.width !== null && this.viewPortSize.height !== null)
      this.viewport(this.viewPortSize.width, this.viewPortSize.height);
    else
		  this.viewport(this.isMobile? 360:1024, this.isMobile?1200:768);

    this.initFlag = true;

	  this.echo(" [GHOST] *init* - Browser has just been initialized ! ");
	}
 };



Ghost.prototype.setViewport =
/** @lends Ghost */
function(width, height) {

	width = (width===undefined || width===null)? 2560 : width;
	height = (height===undefined || height===null)? 1600 : height;

	this.viewport(width, height);
};


/**
 * Stop script execution, just after a given last exit message is displayed
 * @param {string} msg - The message to display before exiting
 */
Ghost.prototype.thenExit =
/** @lends Ghost */
function(msg) {

  this.then(function() {
    this.echo(msg);
  });

  this.then(function() {
    phantom.exit();
  });
};


/**
 * Init the browser, open a web page, given a specific url, and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {string} screenName - The screenshot file name
 */
Ghost.prototype.init =
/** @lends Ghost */
function(url, screenName) {

  console.log("[GHOSTJS] *init* - Entering init...");

  this.initBrowser();

  // Open page + WAIT + SCREEN
  if (url !== null && url !== undefined)
    this.thenOpenThenWaitAndScreen(url, 1000 * this.initWaitTime, screenName); 

  this.thenEcho(" [GHOSTJS] *init* - Init done. ");
};



/**
 * Init browser, open web page given a specific url, wait while an element is visible (identified by its XPath), and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {string} xpath - XPath identifying the element to wait for while visible
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.initThenWaitWhileVisible =
/** @lends Ghost */
function(url, xpath, screenName) {

	this.initBrowser();

  // Open page + WAIT WHILE VISIBLE + SCREEN
  this.thenOpenThenWaitWhileVisibleAndScreen(url, xpath, screenName);
};



/**
 * Init browser, open web page given a specific url, wait for an element to be visible (identified by its XPath), and take a screenshot
 * @param {string} url - Page URL to be opened
 * @param {string} xpath - XPath identifying the element to wait for
 * @param {string} screenName - The screenshot file name
 */
 Ghost.prototype.initThenWaitFor =
/** @lends Ghost */
function(url, xpath, screenName) {

	this.initBrowser();

  // Open page + WAIT FOR SELECTOR + SCREEN
  this.thenOpenThenWaitForXPathAndScreen(url, xpath, screenName);
};


/**
 *
 */
Ghost.prototype.solveCaptcha = function(cookieFilename) {

  this.thenWait(2,4, 1000);

    if(this.exists(x(this.xpaths.captcha.image))) {

      // Save captcha image to file
      this.thenCaptureSel(this.xpaths.captcha.image, "img_captcha_");

      // Send Captcha detected notification to PHP
      this.thenWriteLineToStdout("[ GHOSTJS ] - !CAPTCHA! has been detected ! Waiting for user input...");

      // SOLVE CAPTCHA
      this.then(function(){

        var line = null;

        // Wait for user input, from PHP cmd line
        this.then(function(){

          line = system.stdin.readLine();
          system.stdout.writeLine("[ GHOSTJS ] - Solved Captcha: "+ JSON.stringify(line));


          // Fill Captcha field
        this.thenSendKeys(this.xpaths.captcha.challengeInput, line);

        // Wait a few seconds before clicking on SUBMIT
        this.thenWaitAndScreen(1,2, 1000, "captcha_filled");

        // Click on the SUBMIT button
          this.thenClick(x(this.xpaths.captcha.submitButton), function() {

        this.echo(" [GHOSTJS] *solveCaptcha* - I've clicked on button/link: " + this.xpaths.captcha.submitButton);

        this.thenWaitAndScreen(2,3, 1000, "after_captcha_submit");
        this.thenSaveCookies(cookieFilename);

      });

        });        

      }); // END THEN

      this.thenSaveCookies(cookieFilename);

    } // END IF SELECTOR EXISTS
 
}; // END IF SOLVE CAPTCHA


module.exports = {
	Casper: Ghost
};