/*jshint strict:false*/
/*jshint esnext:true*/

// my module, stored in universe.js
// patching phantomjs' require()
//var require = patchRequire(require);

// now you're ready to go
//var utils = require('utils');

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomUniqueIntegers(min, max, count) {

	var arr = [];

	while(arr.length < count){

	  var randomnumber=getRandomInt(min, max);
	  //var randomnumber=Math.floor(Math.random() * (max - min + 1)) + min;
	  var found=false;

	  for(var i=0;i<arr.length;i++){
		if(arr[i]==randomnumber){found=true;break;}
	  }

	  if(!found)arr[arr.length]=randomnumber;
	}

	return arr;
}

function shuffle(array) {
	
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

/*
function getRandomArrayElement(array) {

	var max = array.length;
	return array[getRandomInt(0,max-1)];
}*/



module.exports = {
	getRandomInt: getRandomInt,
	getRandomUniqueIntegers: getRandomUniqueIntegers,
	shuffleArray: shuffle
};